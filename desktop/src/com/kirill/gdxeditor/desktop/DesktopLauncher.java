package com.kirill.gdxeditor.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.kirill.gdxeditor.Editor;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 1120;
		config.height = 630;
		config.samples = 8;
		config.stencil = 2;

		LwjglApplication app = new LwjglApplication(new Editor(arg), config);

		boolean needExist = false;
		for(String a : arg){
			Gdx.app.log("Args", a);
			needExist = true;
		}

		if (needExist) {
			app.exit();
		}
	}
}
