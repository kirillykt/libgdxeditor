

varying vec2 position;
uniform float time;
uniform sampler2D u_texture;
uniform sampler2D u_mask;
varying vec4 v_color;
varying vec2 v_texCoords;

uniform vec2 center;

const float PI = 3.1415926535;
uniform vec2 resolution;
uniform vec2 mouse;
uniform float lensSize;
vec2 Distort(vec2 p)
{
    float theta  = atan(p.y, p.x);
    float radius = length(p);
    radius = pow(radius, 1.4);
    p.x = radius * cos(theta);
    p.y = radius * sin(theta);
    return 0.5 * (p + 1.0);
}

uniform float rt_w; // render target width
uniform float rt_h; // render target height
uniform float vx_offset;



void blur(){
    vec4 sum = vec4(0.0);
    vec2 tc = v_texCoords;
    float blur = vx_offset/rt_w;

    float hstep = 1.0;
    sum += texture2D(u_texture, vec2(tc.x - 4.0*blur*hstep, tc.y)) * 0.05;
    sum += texture2D(u_texture, vec2(tc.x - 3.0*blur*hstep, tc.y)) * 0.09;
    sum += texture2D(u_texture, vec2(tc.x - 2.0*blur*hstep, tc.y)) * 0.12;
    sum += texture2D(u_texture, vec2(tc.x - 1.0*blur*hstep, tc.y)) * 0.15;

    sum += texture2D(u_texture, vec2(tc.x, tc.y)) * 0.16;

    sum += texture2D(u_texture, vec2(tc.x - 1.0*blur*hstep, tc.y)) * 0.15;
    sum += texture2D(u_texture, vec2(tc.x - 2.0*blur*hstep, tc.y)) * 0.12;
    sum += texture2D(u_texture, vec2(tc.x - 3.0*blur*hstep, tc.y)) * 0.09;
    sum += texture2D(u_texture, vec2(tc.x - 4.0*blur*hstep, tc.y)) * 0.05;

    gl_FragColor = v_color * vec4(sum.rgb, 1.0);
}



vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v)
  {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

float getOffset(float tmp, float step){
    float temp = tmp;
    if(temp > 1000.0){
         float t = temp / 1000.0;
         int iT = int(t);
         iT *= 1000;
         temp -= float(iT);
    }
    if(temp > 100.0){
        float t = temp / 100.0;
        int iT = int(t);
        iT *= 100;
        temp -= float(iT);
    }
    if(temp > 10.0){
        float t = temp / 10.0;
        int iT = int(t);
        iT *= 10;
        temp -= float(iT);
    }
    int iTemp = int(temp);

    float offset = -0.03 + iTemp * step;
    return offset;
}

float mynoise(float x, float y, float time, float lens, float angle){

       float offset;
       //offset += getOffset(angle, 0.005);
       float val = 0.02 * sin(angle * 0.17) * sin(time / 2);
       offset += getOffset(x, 0.005) * cos(time);
       offset += getOffset(y, 0.005) * sin(time);
       offset *= snoise(vec2(x, y)) * 0.8;

       lens += val;
       lens += offset;
       return lens;
}



float lerp(float a, float b, float w){
    return a + w*(b - a);
}

void main() {
  /*vec2 xy = 2.0 * v_texCoords - 1.0;
  vec2 uv;
  float d = length(xy);
  if (d < 1.0)
  {
    uv = Distort(xy);
  }
  else
  {
    uv = v_texCoords.xy;
  }
  vec4 c = texture2D(u_texture, uv);
  gl_FragColor = c;*/


//////////////////
  vec2 p = gl_FragCoord.xy / resolution.xy;
  vec2 m = mouse.xy / resolution.xy;


  vec2 d = p - m;
  float r = sqrt(dot(d, d)); // distance of pixel from mouse

  vec2 uv;

  float deltaX = mouse.x - gl_FragCoord.x;
  float deltaY = mouse.y - gl_FragCoord.y;
  float angle = atan(deltaX, deltaY) * 180 / 3.14159265;
  if (r >= mynoise(gl_FragCoord.x, gl_FragCoord.y, time, lensSize, angle)) {
    //uv = p;
    blur();
  } else {
      // Thanks to Paul Bourke for these formulas; see
      // http://paulbourke.net/miscellaneous/lenscorrection/
      // and .../lenscorrection/lens.c
      // Choose one formula to uncomment:
      // SQUAREXY:
      // uv = m + vec2(d.x * abs(d.x), d.y * abs(d.y));
      // SQUARER:
      //uv = m + d * r; // a.k.a. m + normalize(d) * r * r
      // SINER:
      uv = m + normalize(d) * sin(r * 3.14159265 * 0.31);
      // ASINR:
      //uv = m + normalize(d) * asin(r) / (3.14159265 * 0.5);
      vec3 col = texture2D(u_texture, vec2(uv.x, -uv.y)).xyz;

      gl_FragColor = vec4(col, 1.0);
  }




/////////////////////////
}