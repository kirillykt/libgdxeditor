package com.kirill.gdxeditor.input;

public interface SimpleInputListener {
	
	public boolean touchDown(float x, float y);
	public boolean touchUp(float x, float y);
	public void touchDragged(float x, float y, float deltaX, float deltaY);

}
