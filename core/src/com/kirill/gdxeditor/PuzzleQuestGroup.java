package com.kirill.gdxeditor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;

/**
 * Created by Kirill on 06.11.2015.
 */
public class PuzzleQuestGroup extends Group {

    Image finish;
    Image[] images;
    Vector2[] finalPos;
    Texture[] textures;
    Array<XmlReader.Element> elements;

    int size = 4;
    String path = "puzzle/";

    final float NORTH = 0, EAST = 90, SOUTH = 180, WEST = 270, STEP = 90;

    public PuzzleQuestGroup(){
        super();

        finish = new Image(new Texture(path + "final.png"));
        elements = new Array<XmlReader.Element>();
        XmlReader reader = new XmlReader();
        try {
            XmlReader.Element root = reader.parse(Gdx.files.internal(path + "puzzle.xml"));
            for(int i = 0; i < root.getChildCount(); i++){
                XmlReader.Element child = root.getChild(i);
                String name = child.get("name");
                String[] nameParts = name.split("_");
                if(nameParts[0].equals("piece")){
                    addNewPiece(child);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        images = new Image[elements.size];
        textures = new Texture[elements.size];
        finalPos = new Vector2[elements.size];


        for(int i = 0; i < elements.size; i++){
            textures[i] = new Texture(path + elements.get(i).getText());
            images[i] = new Image(textures[i]);
            finalPos[i] = new Vector2(elements.get(i).getFloat("x"), 2048 - elements.get(i).getFloat("y") - images[i].getHeight());
            images[i].addListener(new InputListener(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return super.touchDown(event, x, y, pointer, button);
                }
            });
        }
    }

    void addNewPiece(XmlReader.Element pieceElement){
        elements.add(pieceElement);
    }

}
