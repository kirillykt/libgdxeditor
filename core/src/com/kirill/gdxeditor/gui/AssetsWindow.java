package com.kirill.gdxeditor.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kirill.gdxeditor.Editor;
import com.kirill.gdxeditor.gui.tabs.AnimationsTab;
import com.kirill.gdxeditor.gui.tabs.ParticlesTab;
import com.kirill.gdxeditor.gui.tabs.SpritesTab;
import com.kirill.gdxeditor.screens.EditorScreen;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPane;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPaneAdapter;

import java.io.File;
import java.io.FileFilter;

/**
 * Created by Kirill on 06.06.2015.
 */
public class AssetsWindow extends VisWindow {

    VisTextButton browseButton;
    VisScrollPane assetsPane;
    public ObjectMap<String, VisImageButton> assetsButtons;
    FileChooser fileChooser;
    public ObjectMap<String, FileHandle> assets;


    String favoritesPath = "com.kirillka.gdxeditor";

    float assetsPaneWidth = EditorScreen.assetsPaneWidth, assetsPaneHeight = EditorScreen.assetsPaneHeight;
    float padding = EditorScreen.padding;

    boolean fileChooserOpened;
    EditorScreen editorScreen;

    TabbedPane tabbedPane;
    SpritesTab spritesTab;
    AnimationsTab animationsTab;
    ParticlesTab particlesTab;

    enum MODE{SPRITES, ANIMATIONS};
    MODE mode = MODE.SPRITES;


    public AssetsWindow(EditorScreen editorScreen) {
        super("Assets");
        this.editorScreen = editorScreen;

        assetsButtons = new ObjectMap<String, VisImageButton>();
        assets = new ObjectMap<String, FileHandle>();

        align(Align.topLeft);

        createActors();
        setupActors();
        addActors();
    }

    private void createActors() {
        tabbedPane = new TabbedPane();
        spritesTab = new SpritesTab();
        animationsTab = new AnimationsTab();
        particlesTab = new ParticlesTab();

        assetsPane = new VisScrollPane(spritesTab.getContentTable());
        assetsPane.setScrollingDisabled(true, false);
        assetsPane.layout();


        browseButton = new VisTextButton("Browse");

        FileChooser.setFavoritesPrefsName(favoritesPath);
        fileChooser = new FileChooser(FileChooser.Mode.OPEN);
    }

    private void addActors() {
        add(tabbedPane.getTable()).left().row();
        add(assetsPane).expand().fill().row();
        add(browseButton).left();
    }

    private void setupActors() {

        tabbedPane.add(spritesTab);
        tabbedPane.add(animationsTab);
        tabbedPane.add(particlesTab);
        tabbedPane.setAllowTabDeselect(false);
        tabbedPane.switchTab(spritesTab);
        tabbedPane.addListener(new TabbedPaneAdapter(){
            @Override
            public void switchedTab(Tab tab) {
                if(tab.getTabTitle().equals(SpritesTab.title)){
                    assetsPane.setWidget(spritesTab.getContentTable());
                }

                else if(tab.getTabTitle().equals(AnimationsTab.title)){
                    assetsPane.setWidget(animationsTab.getContentTable());
                }

                else if(tab.getTabTitle().equals(ParticlesTab.title)){
                    assetsPane.setWidget(particlesTab.getContentTable());
                }
            }
        });

        assetsPane.setSize(EditorScreen.assetsPaneWidth, EditorScreen.assetsPaneHeight);
        assetsPane.setFadeScrollBars(false);

        setPosition(Editor.W - assetsPaneWidth - padding, padding);
        setSize(assetsPaneWidth, assetsPaneHeight);
        fileChooser.setColor(1, 1, 1, 0);
        browseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getStage().addActor(fileChooser.fadeIn());
                fileChooserOpened = true;
            }
        });

        fileChooser.setSelectionMode(FileChooser.SelectionMode.FILES);
        fileChooser.setMultiselectionEnabled(true);

        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.toString().contains(".png") || file.toString().contains(".jpg") || file.isDirectory())
                    return true;
                else return false;
            }
        });

        fileChooser.setListener(new FileChooserAdapter() {
            @Override
            public void selected(Array<FileHandle> files) {
                switch (mode){
                    case SPRITES:
                        processSprites(files);
                        break;
                    default:
                        break;
                }
            }

        });
    }

    void processSprites(Array<FileHandle> files){
        for (int i = 0; i < files.size; i++)
            if (!assets.containsKey(files.get(i).name())) {
                Preferences prefs = Gdx.app.getPreferences(favoritesPath);
                if (!prefs.contains(files.get(i).parent().toString())) {
                    prefs.putString(files.get(i).parent().toString(), files.get(i).parent().toString());
                    fileChooser.addFavorite(files.get(i).parent());
                }
                assets.put(files.get(i).name(), files.get(i));
                assetsButtons.put(files.get(i).name(), new VisImageButton(
                        new TextureRegionDrawable(
                                new TextureRegion(new Texture(files.get(i))))));
                spritesTab.getContentTable().add(assetsButtons.get(files.get(i).name())).top().left().width(150).height(100);
                spritesTab.getContentTable().add(new VisTextButton(files.get(i).nameWithoutExtension())).top().left().padLeft(10f).row();
                final FileHandle spriteHandle = files.get(i);
                spritesTab.getContentTable().layout();
                spritesTab.getContentTable().pack();
                assetsPane.layout();
                assetsPane.pack();
                assetsButtons.get(files.get(i).name()).addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x,
                                        float y) {
                        Sprite sprite = new Sprite(new Texture(spriteHandle));
                        editorScreen.entityManager.createNewEntity(sprite);
                        editorScreen.sprites.add(sprite);
                    }
                });
            }
    }
}
