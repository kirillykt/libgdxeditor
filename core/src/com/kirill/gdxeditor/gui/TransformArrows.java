package com.kirill.gdxeditor.gui;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.kirill.gdxeditor.ashley.components.TransformComponent;
import com.kirill.gdxeditor.input.SimpleInputListener;
import com.kirill.gdxeditor.screens.EditorScreen;

public class TransformArrows implements SimpleInputListener{
	
	public Texture xArrrowTexture, yArrowTexture;
	public Sprite xArrow, yArrow;
	public Entity currentEntity;
	boolean selectedX = false, selectedY = false;
	boolean isActive = false;
	
	public TransformArrows(EditorScreen editorScreen) {
		xArrrowTexture = new Texture("textures/x_arrow.png");
		yArrowTexture = new Texture("textures/y_arrow.png");
		xArrow = new Sprite(xArrrowTexture);
		yArrow = new Sprite(yArrowTexture);
		editorScreen.registerSimpleInputListener(this);
	}
	
	float offset = 30f;
	
	public void draw(SpriteBatch batch, Entity entity, float cameraZoom){
		currentEntity = entity;
		TransformComponent transform = entity.getComponent(TransformComponent.class);
		//xArrow.setSize(xArrow.getWidth() * cameraZoom, xArrow.getHeight() * cameraZoom);
		//yArrow.setSize(yArrow.getWidth() * cameraZoom, yArrow.getHeight() * cameraZoom);
		xArrow.setScale(cameraZoom);
		yArrow.setScale(cameraZoom);
		
		xArrow.setPosition(transform.position.x - offset, transform.position.y - offset - xArrow.getHeight() / 2);
		yArrow.setPosition(xArrow.getX() - yArrow.getWidth() / 2, xArrow.getY() + yArrow.getHeight() / 2);
		
		
		xArrow.draw(batch);
		yArrow.draw(batch);
	}
	
	public boolean touchDown(float x, float y){
		Gdx.app.log("TD", "x = " + x + " y = " + y);
		if(!isActive()){
			selectedX = false;
			selectedY = false;
			return false;
		}
		if(xArrow.getBoundingRectangle().contains(x, y)) {
			selectedX = true;
			selectedY = false;
			return true;
		}
		else if(yArrow.getBoundingRectangle().contains(x, y)) {
			selectedY = true;
			selectedX = false;
			return true;
		}
		
		else{
			selectedX = false;
			selectedY = false;
			return false;
		}
	}
	
	public boolean isSelected(){
		if(!isActive()) return false;
		return selectedX || selectedY ? true: false;
	}

	@Override
	public boolean touchUp(float x, float y) {
		selectedX = false;
		selectedY = false;
		setActive(false);
		return false;
	}
	
	public void setActive(boolean active){
		isActive = active;
	}
	
	public boolean isActive(){
		return isActive;
	}

	@Override
	public void touchDragged(float x, float y, float deltaX, float deltaY) {
		if(!isActive()) return;
		
		if(selectedX){
			if(currentEntity != null){
				currentEntity.getComponent(TransformComponent.class).position.x -= deltaX;
			}
		}
		
		else if(selectedY){
			if(currentEntity != null){
				currentEntity.getComponent(TransformComponent.class).position.y -= deltaY;
			}
		}
	}

}
