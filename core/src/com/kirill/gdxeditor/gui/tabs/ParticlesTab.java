package com.kirill.gdxeditor.gui.tabs;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

/**
 * Created by Kirill on 06.06.2015.
 */
public class ParticlesTab extends Tab {
    public static String title = "Particles";
    VisTable content;
    public ParticlesTab(){
        super(false, false);

        content = new VisTable();
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public Table getContentTable() {
        return content;
    }
}
