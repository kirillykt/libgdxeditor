package com.kirill.gdxeditor.gui.tabs;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

/**
 * Created by Kirill on 06.06.2015.
 */
public class SpritesTab extends Tab{
    VisTable assetsTable;

    public static String title = "Sprites";

    public SpritesTab(){
        super(false, false);
        assetsTable = new VisTable();
        assetsTable.top().left();

    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public Table getContentTable() {
        return assetsTable;
    }
}
