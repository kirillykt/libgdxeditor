package com.kirill.gdxeditor.gui.tabs;

import com.kotcrab.vis.ui.widget.VisScrollPane;

/**
 * Created by Kirill on 06.06.2015.
 */
public interface ScrollPaneHolder {

    VisScrollPane getContentPane();
}
