package com.kirill.gdxeditor.gui;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DecimalFormat;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.kirill.gdxeditor.Editor;
import com.kirill.gdxeditor.ashley.EntityManager;
import com.kirill.gdxeditor.ashley.components.TouchableComponent;
import com.kirill.gdxeditor.ashley.systems.RenderingSystem;
import com.kirill.gdxeditor.screens.EditorScreen;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisWindow;

public class InspectorWindow extends VisWindow{
	float assetsPaneWidth = EditorScreen.assetsPaneWidth, assetsPaneHeight = EditorScreen.assetsPaneHeight,
			menuBarHeight = EditorScreen.menuBarHeight, padding = EditorScreen.padding;

	EntityManager entityManager;
	Entity currentEntity;
	Table table;
	VisScrollPane componentsPane;
	VisLabel transformComponentLabel;
	Array<TextFieldFloatListener> floatListeners;
	Array<TextFieldIntegerListener> integerListeners;
	
	public InspectorWindow(String title, EntityManager entityManager) {
		super(title);
		this.entityManager = entityManager;
		setSize(assetsPaneWidth, Editor.H - assetsPaneHeight - menuBarHeight - padding * 4);
		setPosition(Editor.W - getWidth() - padding, Editor.H - menuBarHeight - getHeight() - padding);
		floatListeners = new Array<TextFieldFloatListener>();
		integerListeners = new Array<TextFieldIntegerListener>();
		createActors();
		setupActors();
		addActors();
	}
	
	private void createActors(){
		table = new Table();
		table.setWidth(getWidth());
		componentsPane = new VisScrollPane(table);
		
		transformComponentLabel = new VisLabel("Transform");
	}
	
	private void setupActors() {
		componentsPane.setScrollingDisabled(true, false);
		table.defaults().pad(5f).width(75f);
	}
	
	private void addActors(){
		add(componentsPane).expand().fill();
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(currentEntity != null){
			for(int i = 0; i < floatListeners.size; i++){
				floatListeners.get(i).update();
			}

			for(int i = 0; i < integerListeners.size; i++){
				integerListeners.get(i).update();
			}
		}
	}
	
	public void onEntitySelect(Entity entity){
		currentEntity = entity;
		readFields(entity);
	}
	
	float labelWidth = 125f;
	float textFieldWidth = 125f;
	float elementsPad = 5f;
	private void readFields(Entity entity){
		table.clear();
		floatListeners.clear();
		integerListeners.clear();
		for(int i = 0; i < entity.getComponents().size(); i++){
			if(entity.getComponents().get(i) instanceof TouchableComponent) continue;
			Class clazz = entity.getComponents().get(i).getClass();

			VisWindow componentWindow = new VisWindow(clazz.getSimpleName());
			componentWindow.setMovable(false);
			componentWindow.left();
			componentWindow.align(Align.topLeft);
			componentWindow.defaults().left().colspan(3).pad(elementsPad);

			final Field[] fields = clazz.getFields();
			for(int j = 0; j < fields.length; j++){
				if(Modifier.isPublic(fields[j].getModifiers())){
					String fieldName = fields[j].getType().getName();
					Gdx.app.log("inspector", fieldName);
					if(fieldName.equals("float")){
						componentWindow.add(new VisLabel(fields[j].getName())).width(labelWidth);
						try{
							VisTextField floatTextField = new VisTextField(fields[j].getFloat(entity.getComponents().get(i)) + "");
							floatTextField.setTextFieldListener(new FloatChangeListener(floatTextField, fields[j], entity.getComponents().get(i)));
							floatTextField.setTextFieldFilter(new FloatChangeFilter());
							floatListeners.add(new TextFieldFloatListener(floatTextField, fields[j].getFloat(entity.getComponents().get(i)),
									fields[j], entity.getComponents().get(i)));
							componentWindow.add(floatTextField).width(textFieldWidth).row();
						}
						catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (SecurityException e) {
							e.printStackTrace();
						}
					}
					
					else if(fieldName.equals(Vector2.class.getName())){
						componentWindow.add(new VisLabel(fields[j].getName())).width(labelWidth);
						try {
							Vector2 fieldVector2 = (Vector2)fields[j].get(entity.getComponents().get(i));
							Field xField = Vector2.class.getField("x");
							Field yField = Vector2.class.getField("y");
							VisTextField xTextField = new VisTextField("" + fieldVector2.x);
							VisTextField yTextField = new VisTextField("" + fieldVector2.y);
							xTextField.setTextFieldListener(new FloatChangeListener(xTextField, xField, fieldVector2));
							yTextField.setTextFieldListener(new FloatChangeListener(yTextField, yField, fieldVector2));
							floatListeners.add(new TextFieldFloatListener(xTextField, xField.getFloat(fieldVector2), xField, fieldVector2));
							floatListeners.add(new TextFieldFloatListener(yTextField, yField.getFloat(fieldVector2), yField, fieldVector2));
							componentWindow.add(xTextField).width(textFieldWidth);
							componentWindow.add(yTextField).width(textFieldWidth).row();
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (NoSuchFieldException e) {
							e.printStackTrace();
						} catch (SecurityException e) {
							e.printStackTrace();
						}
					}

					else if(fieldName.equals("int")){
						componentWindow.add(new VisLabel(fields[j].getName())).width(labelWidth);
						try{
							VisTextField intTextField = new VisTextField(fields[j].getInt(entity.getComponents().get(i)) + "");
							intTextField.setTextFieldListener(new IntegerTextFieldListener(intTextField, fields[j], entity.getComponents().get(i)));
							intTextField.setTextFieldFilter(new IntegerTextFieldFilter());
							integerListeners.add(new TextFieldIntegerListener(intTextField, fields[j].getInt(entity.getComponents().get(i)),
									fields[j], entity.getComponents().get(i)));
							componentWindow.add(intTextField).width(textFieldWidth).row();
						}
						catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (SecurityException e) {
							e.printStackTrace();
						}
					}
				}
			}
			componentWindow.layout();
			componentWindow.pack();

			table.add(componentWindow).expandX().fillX().width(table.getWidth()).height(componentWindow.getHeight()).row();
		}
	}
	
	public class FloatChangeListener implements VisTextField.TextFieldListener{
		VisTextField textField;
		Field targetField;
		Object targetObject;
		public FloatChangeListener(VisTextField textField, Field targetField, Object targetObject){
			this.textField = textField;
			this.targetField = targetField;
			this.targetObject = targetObject;
		}

		@Override
		public void keyTyped(VisTextField textField, char c) {
			try {
				if(!textField.getText().isEmpty())
					targetField.setFloat(targetObject, Float.parseFloat(textField.getText()));
			} catch (NumberFormatException e) {
				e.printStackTrace();
				try {
					textField.setText(targetField.getFloat(targetObject) + "");
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			Gdx.app.log("Float changed", textField.getText());
		}
	}


	
	public class TextFieldFloatListener{
		float value;
		Field target;
		Object targetObject;
		VisTextField textField;
		
		public TextFieldFloatListener(VisTextField textField,float initialValue, Field target, Object targetObject) {
			value = initialValue;
			this.textField = textField;
			this.target = target;
			this.targetObject = targetObject;
		}
		
		public void update(){
			float currentValue;
			try {
				currentValue = target.getFloat(targetObject);
				if(value != currentValue && textField.getStage().getKeyboardFocus() != textField){
					textField.setText(currentValue + "");
					value = currentValue;
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	public class FloatChangeFilter implements VisTextField.TextFieldFilter{
		@Override
		public boolean acceptChar(VisTextField textField, char c) {
			if(Character.isDigit(c) || c == '.' || c == '-') return true;
			return false;
		}
	}

	public class IntegerTextFieldListener implements  VisTextField.TextFieldListener{
		VisTextField textField;
		Field targetField;
		Object targetObject;
		public IntegerTextFieldListener(VisTextField textField, Field targetField, Object targetObject){
			this.textField = textField;
			this.targetField = targetField;
			this.targetObject = targetObject;
		}

		@Override
		public void keyTyped(VisTextField textField, char c) {
			try {
				if(!textField.getText().isEmpty()) {
					targetField.setInt(targetObject, Integer.parseInt(textField.getText()));
					entityManager.engine.getSystem(RenderingSystem.class).forceSort();
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
				try {
					textField.setText(targetField.getInt(targetObject) + "");
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			Gdx.app.log("Float changed", textField.getText());
		}
	}

	public class IntegerTextFieldFilter implements  VisTextField.TextFieldFilter{
		@Override
		public boolean acceptChar(VisTextField textField, char c) {
			if(Character.isDigit(c) || c == '-') return true;

			return false;
		}
	}

	public class TextFieldIntegerListener{
		int value;
		Field target;
		Object targetObject;
		VisTextField textField;

		public TextFieldIntegerListener(VisTextField textField,int initialValue, Field target, Object targetObject) {
			value = initialValue;
			this.textField = textField;
			this.target = target;
			this.targetObject = targetObject;
		}

		public void update(){
			int currentValue;
			try {
				currentValue = target.getInt(targetObject);
				if(value != currentValue && textField.getStage().getKeyboardFocus() != textField){
					textField.setText(currentValue + "");
					value = currentValue;

				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

}
