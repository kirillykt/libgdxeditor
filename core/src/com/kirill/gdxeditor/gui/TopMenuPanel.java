package com.kirill.gdxeditor.gui;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ObjectMap;
import com.kirill.gdxeditor.Editor;
import com.kotcrab.vis.ui.widget.Menu;
import com.kotcrab.vis.ui.widget.MenuBar;
import com.kotcrab.vis.ui.widget.MenuItem;

public class TopMenuPanel extends Table{
	
	MenuBar menuBar;
	Menu fileMenu;
	ObjectMap<String, MenuItem> menuItems;
	MenuItem newFileItem;
	MenuItem saveProjectItem;
	MenuItem openProjectItem;
	MenuItem newSceneItem;
	MenuItem saveSceneItem;
	MenuItem openSceneItem;
	MenuItem exitItem;
	
	public float H = 30f, padding = 5f;
	
	public TopMenuPanel() {
		setSize(Editor.W, H);
		setPosition(0, Editor.H - H);
		
		createActors();
		setupActors();
		addActors();
	}
	
	private void createActors(){
		menuBar = new MenuBar();
		fileMenu = new Menu("File");
		
		menuItems = new ObjectMap<String, MenuItem>();
		newFileItem = createMenuItem("New Project");
		saveProjectItem = createMenuItem("Save Project");
		openProjectItem = createMenuItem("Open Project");
		newSceneItem = createMenuItem("New Scene");
		saveSceneItem = createMenuItem("Save Scene");
		openSceneItem = createMenuItem("Open Scene");
		exitItem = createMenuItem("Exit");
		
	}
	
	private void setupActors(){
		exitItem.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});
		fileMenu.defaults().pad(padding);
	}
	
	private void addActors(){
		menuBar.getTable().defaults().width(75f).height(40f).pad(10).left();
		menuBar.addMenu(fileMenu);
		
		Iterator<MenuItem> iterator = menuItems.values().iterator();
		while(iterator.hasNext()){
			fileMenu.addItem(iterator.next());
		}
		
		add(menuBar.getTable()).expand().fill();
	}
	

	
	MenuItem createMenuItem(String title){
		MenuItem item = new MenuItem(title);
		menuItems.put(title, item);
		return item;
	}

}
