package com.kirill.gdxeditor;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.utils.ObjectMap;
import com.kirill.gdxeditor.screens.*;
import com.kirill.gdxeditor.utils.XmlReparser;
import com.kotcrab.vis.ui.VisUI;

public class Editor extends Game {
	private String[] args;

	ObjectMap<Class<? extends Screen>, Screen> screens;
	boolean sceneLoaded = false;
	public String path = "locker/seif.g3dj";
	public static final float W = 1920f, H = 1080f;

	public Editor(String[] args){
		this.args = args;
	}

	@Override
	public void create() {
		Env.init(this);
		VisUI.load(Gdx.files.internal("skin/visui/uiskin.json"));
		VisUI.getSkin().get("default-font", BitmapFont.class).getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

		
		screens = new ObjectMap<Class<? extends Screen>, Screen>();
		screens.put(EditorScreen.class, new EditorScreen());
		screens.put(ItemCreatorScreen.class, new ItemCreatorScreen());
		screens.put(LocationEditor.class, new LocationEditor(args));
		screens.put(HintShader.class, new HintShader());
		screens.put(ThreeDScene.class, new ThreeDScene());
		screens.put(RareItemsCreator.class, new RareItemsCreator());
		//screens.put(PseudoRandomNumberGeneratorTest.class, new PseudoRandomNumberGeneratorTest());

		//setScreen(ItemCreatorScreen.class);
		//setScreen(PseudoRandomNumberGeneratorTest.class);
		//setScreen(EditorScreen.class);
		setScreen(LocationEditor.class);
		//setScreen(HintShader.class);
		//setScreen(ThreeDScene.class);
		//setScreen(RareItemsCreator.class);
		//new XmlReparser();
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {
		//((ItemCreatorScreen)getScreen(ItemCreatorScreen.class)).save();
		screens.get(RareItemsCreator.class).dispose();
		super.dispose();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}



	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}
	
	public void setScreen(Class<? extends Screen> screen){
		setScreen(screens.get(screen));
	}
	
	public Screen getScreen(Class<? extends Screen> screen){
		return screens.get(screen);
	}

	public LocationEditor getLocationEditor(){
		return (LocationEditor)screens.get(LocationEditor.class);
	}
	
}
