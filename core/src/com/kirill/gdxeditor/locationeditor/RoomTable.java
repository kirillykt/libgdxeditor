package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextField;

/**
 * Created by Kirill on 14.10.2015.
 */
public class RoomTable extends VisTable {

    VisLabel roomNameLabel;
    VisTextField roomNameTextField;
    RoomNode roomNode;

    public RoomTable(final RoomNode roomNode){
        this.roomNode = roomNode;
        roomNameTextField = new VisTextField(roomNode.getRoomName());
        roomNameTextField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                roomNode.setRoomName(textField.getText());
            }
        });
        defaults().pad(10).space(10);
        align(Align.topLeft);
        roomNameLabel = new VisLabel("Name");
        add(roomNameLabel);
        add(roomNameTextField);
    }
}
