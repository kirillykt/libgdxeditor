package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;

/**
 * Created by Kirill on 16.10.2015.
 */
public class ObstacleTable extends VisTable {

    ObstacleNode obstacleNode;
    VisCheckBox keyCheckBox, crowbarCheckBox, blockedCheckBox;
    VisTextField blocksTextField;

    public ObstacleTable(ObstacleNode obstacle){
        this.obstacleNode = obstacle;
        keyCheckBox = new VisCheckBox("Key", false);
        keyCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                obstacleNode.setKey(keyCheckBox.isChecked());
            }
        });

        crowbarCheckBox = new VisCheckBox("Crowbar", false);
        crowbarCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                obstacleNode.setCrowbar(crowbarCheckBox.isChecked());
            }
        });
        blockedCheckBox = new VisCheckBox("Blocked", false);
        blockedCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                obstacleNode.setBlocked(blockedCheckBox.isChecked());
            }
        });

        blocksTextField = new VisTextField("Blocks");
        blocksTextField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                obstacleNode.setBlocks(blocksTextField.getText());
            }
        });

        align(Align.topLeft);
        defaults().pad(5).space(5).left();
        add(keyCheckBox).row();
        add(crowbarCheckBox).row();
        add(blockedCheckBox).row();
        add(blocksTextField).row();
    }
}
