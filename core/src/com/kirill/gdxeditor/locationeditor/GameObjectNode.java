package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 20.10.2015.
 */
public class GameObjectNode extends Tree.Node {
    private String objName;
    public float posX, posY;
    public String variation;

    public GameObjectNode(String name){
        super(new VisLabel(name));
        objName = name;
    }

    public String getName() {
        return objName;
    }
}
