package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.kotcrab.vis.ui.widget.VisLabel;

public class GemNode extends Tree.Node {
    private String name;
    private int variation;
    private float x;
    private float y;
    private boolean defaultVisible = true;

    //region initialization
    public GemNode(String name, int variation, float x, float y){
        super(new VisLabel(name));

        this.name = name;
        this.variation = variation;
        this.x = x;
        this.y = y;
    }

    public void setDefaultInvisible() {
        defaultVisible = false;
    }
    //endregion

    //region getters
    public String getName() {
        return name;
    }

    public int getVariation() {
        return variation;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public boolean isDefaultVisible() {
        return defaultVisible;
    }

    //endregion
}
