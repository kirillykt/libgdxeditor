package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 14.10.2015.
 */
public class FrameNode extends Tree.Node {

    private int index;
    VisLabel nodeLabel;
    FrameTable frameTable;
    LocationEditor locationEditor;
    private ObjectMap<String, ObstacleNode> obstacleNodes = new ObjectMap<String, ObstacleNode>();
    private Array<ObstacleNode> obstacleNodesByOrder = new Array<ObstacleNode>();
    Array<ZoomTriggerNode> clickZoneNodes = new Array<ZoomTriggerNode>();
    Array<KeyNode> keyNodes = new Array<KeyNode>();
    private Array<PassageNode> passageNodes = new Array<PassageNode>();
    Array<GameObjectNode> gameObjectNodes = new Array<GameObjectNode>();
    private Array<CoverNode> coverNodes = new Array<CoverNode>();
    private Array<GemNode> gemNodes = new Array<GemNode>();
    private Array<LetterNode> letterNodes = new Array<LetterNode>();
    private Array<PackNode> packNodes = new Array<PackNode>();
    private Array<CoinNode> coinNodes = new Array<CoinNode>();

    //region initialization
    public FrameNode(final int index){
        super(new VisLabel("Frame " + index));
        nodeLabel = (VisLabel)getActor();
        this.index = index;
        frameTable = new FrameTable(this);
        locationEditor = Env.editor.getLocationEditor();
        nodeLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                locationEditor.setComponentsWindowContent(frameTable, "Frame " + index);
            }
        });
    }

    public void setIndex(int index) {
        this.index = index;
        nodeLabel.setText("Frame " + index);
    }

    public void addPassage(PassageNode passageNode){
        this.add(passageNode);
        passageNodes.add(passageNode);
    }

    public void addGood(GameObjectNode goodNode){
        this.add(goodNode);
        gameObjectNodes.add(goodNode);
    }

    public void addGem(GemNode gemNode){
        this.add(gemNode);
        gemNodes.add(gemNode);
    }

    public void addKey(KeyNode keyNode){
        this.add(keyNode);
        keyNodes.add(keyNode);
    }

    public void addObstacle(ObstacleNode obstacleNode){
        this.add(obstacleNode);
        obstacleNodes.put(obstacleNode.getName(), obstacleNode);

        obstacleNodesByOrder.add(obstacleNode);
    }

    public void addZoomTrigger(ZoomTriggerNode zoomTriggerNode){
        this.add(zoomTriggerNode);
        clickZoneNodes.add(zoomTriggerNode);
    }

    public void addCover(CoverNode coverNode){
        this.add(coverNode);
        coverNodes.add(coverNode);
    }

    public void addLetter(LetterNode letterNode){
        this.add(letterNode);
        letterNodes.add(letterNode);
    }

    public void addPack(PackNode packNode) {
        this.add(packNode);
        packNodes.add(packNode);
    }

    public void addCoin(CoinNode coinNode){
        this.add(coinNode);
        coinNodes.add(coinNode);
    }
    //endregion

    //region getters
    public Array<GameObjectNode> getGameObjectNodes(){
        return gameObjectNodes;
    }

    public Array<KeyNode> getKeyNodes() {
        return keyNodes;
    }

    public Array<PassageNode> getPassageNodes() {
        return passageNodes;
    }

    public int getIndex() {
        return index;
    }

    public ObjectMap<String, ObstacleNode> getObstacleNodes(){
        return obstacleNodes;
    }

    public Array<ObstacleNode> getObstacleNodesByOrder() {
        return obstacleNodesByOrder;
    }

    public Array<ZoomTriggerNode> getClickZoneNodes(){
        return clickZoneNodes;
    }

    public Array<CoverNode> getCoverNodes() {
        return coverNodes;
    }

    public ObstacleNode getObstacle(String name){
        return obstacleNodes.get(name);
    }

    public Array<GemNode> getGemNodes() {
        return gemNodes;
    }

    public Array<LetterNode> getLetterNodes() {
        return letterNodes;
    }

    public Array<PackNode> getPackNodes() {
        return packNodes;
    }

    public Array<CoinNode> getCoinNodes() {
        return coinNodes;
    }

    //endregion
}
