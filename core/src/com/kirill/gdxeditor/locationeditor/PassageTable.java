package com.kirill.gdxeditor.locationeditor;

import com.kotcrab.vis.ui.widget.NumberSelector;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextField;

/**
 * Created by Kirill on 15.10.2015.
 */
public class PassageTable extends VisTable {

    PassageNode passageNode;
    NumberSelector selector;
    VisLabel roomNameLabel;
    VisTextField roomNameField;

    public PassageTable(final PassageNode node){
        super();
        passageNode = node;
        selector = new NumberSelector("Frame Index", node.openingFrameIndex, 0, 100, 1);
        selector.addChangeListener(new NumberSelector.NumberSelectorListener() {
            @Override
            public void changed(int number) {
                passageNode.setOpeningFrameIndex(number);
            }
        });
        add(selector).row();

        roomNameLabel = new VisLabel("Room Name");
        roomNameField = new VisTextField(node.getRoomName());
        roomNameField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                node.setRoomName(textField.getText());
            }
        });
        add(roomNameLabel).left();
        add(roomNameField).left();
    }
}
