package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.kotcrab.vis.ui.widget.VisLabel;

public class CoverNode extends Tree.Node  {
    private String name;
    private float x;
    private float y;

    //region initialization
    public CoverNode(String name, float x, float y){
        super(new VisLabel(name));

        this.name = name;
        this.x = x;
        this.y = y;
    }
    //endregion

    //region getters
    public String getName() {
        return name;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
    //endregion
}
