package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

public class NameParser {
    private Array<String> parts = new Array<String>();
    private static String matcher = "[a-zA-Z0-9_-]";

    public void parse(String rawName){
        parts.clear();

        //check
        for(int i = 0; i < rawName.length(); ++i){
            String c = rawName.substring(i, i + 1);
            if (!c.matches(matcher)) {
                Gdx.app.error("ERROR_CONTENT", "Wrong name: " + rawName + " " + c + " at " + i);
            }
        }

        parts = new Array<String>(rawName.split("_"));
    }

    public boolean isPrefixEquals(String str){
        return parts.get(0).equals(str);
    }

    public String getName(){
        StringBuilder result = new StringBuilder(parts.get(1));

        int count = parts.size;
        for(int i = 2; i < count - 1; ++i){
            result.append("_");
            result.append(parts.get(i));
        }

        return result.toString();
    }

    public String getName(int startCut, int endCut){
        StringBuilder result = new StringBuilder(parts.get(startCut));

        int count = parts.size;
        for(int i = startCut + 1; i < count - endCut; ++i){
            result.append("_");
            result.append(parts.get(i));
        }

        return result.toString();
    }

    public String getLongName(){
        StringBuilder result = new StringBuilder(parts.get(1));

        int count = parts.size;
        for(int i = 2; i < count; ++i){
            result.append("_");
            result.append(parts.get(i));
        }

        return result.toString();
    }

    public String getPostfix(){
        return parts.get(parts.size - 1);
    }

    public String getPostfix(int endCut){
        return parts.get(parts.size - 1 - endCut);
    }
}
