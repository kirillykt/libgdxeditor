package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 15.10.2015.
 */
public class PassageNode extends Tree.Node {
    private VisLabel nodeLabel;
    private PassageTable passageTable;
    private LocationEditor locationEditor;
    public float posX, posY, w, h;

    int openingFrameIndex;
    String roomName = "";

    public PassageNode(final String roomName, final int frameIndex) {
        super(new VisLabel("Passage - " + roomName + " - " + frameIndex));
        this.roomName = roomName;
        this.openingFrameIndex = frameIndex;
        locationEditor = Env.editor.getLocationEditor();
        passageTable = new PassageTable(this);

        nodeLabel = (VisLabel)getActor();
        nodeLabel.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                locationEditor.setComponentsWindowContent(passageTable, "Passage " + roomName + " " + frameIndex);
            }
        });
    }

    public void setOpeningFrameIndex(int index){
        openingFrameIndex = index;
        nodeLabel.setText("Passage - " + roomName + " - " + openingFrameIndex);
    }

    public int getOpeningFrameIndex(){
        return openingFrameIndex;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
        nodeLabel.setText("Passage - " + roomName + " - " + openingFrameIndex);
    }
}
