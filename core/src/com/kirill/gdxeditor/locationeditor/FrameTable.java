package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.NumberSelector;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;

/**
 * Created by Kirill on 14.10.2015.
 */
public class FrameTable extends VisTable {

    FrameNode frameNode;
    NumberSelector indexSelector;

    public FrameTable(final FrameNode frameNode){
        this.frameNode = frameNode;
        indexSelector = new NumberSelector("Index", frameNode.getIndex(), 0, 100, 1);
        indexSelector.addChangeListener(new NumberSelector.NumberSelectorListener(){
            @Override
            public void changed(int number) {
                frameNode.setIndex(number);
            }
        });
        align(Align.topLeft);
        defaults().pad(10).space(10).expand();
        add(indexSelector);
    }
}
