package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 14.10.2015.
 */
public class RoomNode extends Tree.Node {

    String roomName;
    LocationEditor locationEditor;
    RoomTable roomTable;
    VisLabel nodeLabel;
    private Array<FrameNode> frames = new Array<FrameNode>();
    private ObjectMap<String, ZoomNode> zooms = new ObjectMap<String, ZoomNode>();

    //region initialization
    public RoomNode(final String text){
        super(new VisLabel("Room - " +text));
        nodeLabel = (VisLabel)getActor();
        locationEditor = (LocationEditor)Env.editor.getScreen(LocationEditor.class);
        setRoomName(text);
        roomTable = new RoomTable(this);
        getActor().addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                locationEditor.setComponentsWindowContent(RoomNode.this.roomTable, "Room " + text);
            }
        });
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
        nodeLabel.setText("Room - " + roomName);
    }

    public void addFrame(FrameNode frameNode){
        this.add(frameNode);
        frames.add(frameNode);

        int insideIndex = frameNode.getIndex();
        int framesIndex = frames.size - 1;
        if (insideIndex != framesIndex){
            Gdx.app.error("ERROR_DESIGN_EDITOR", "RoomNode. Mismatch of inside and frames index:" + insideIndex + "/" + framesIndex);
        }
    }

    public void addZoom(ZoomNode zoomNode){
        Gdx.app.log("RoomNode", "addZoom " + zoomNode.getName());
        this.add(zoomNode);
        zooms.put(zoomNode.getName(), zoomNode);
    }
    //endregion

    //region getters
    public String getRoomName() {
        return roomName;
    }

    public Array<FrameNode> getFrames(){
        return frames;
    }

    public ObjectMap<String, ZoomNode> getZooms() {
        return zooms;
    }

    public ZoomNode getZoom(String zoomName){
        return zooms.get(zoomName);
    }
    //endregion
}
