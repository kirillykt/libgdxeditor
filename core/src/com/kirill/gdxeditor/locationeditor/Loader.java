package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;

import java.io.IOException;

/**
 * Created by Kirill on 15.10.2015.
 */
public class Loader {

    private static Loader instance;
    LocationEditor locationEditor;

    //temporary objects (lifetime: from start loading, to end loading)
    //private LocationNode curLocation;
    private RoomNode curRoom;

    private Loader(){
        locationEditor = Env.editor.getLocationEditor();
    }

    public static Loader getInstance(){
        if(instance == null) instance = new Loader();
        return instance;
    }

    public void load(FileHandle sourceHandle){
        Gdx.app.log("Loader", "Load: " + sourceHandle.name());
        createLocation(sourceHandle);
        FileHandle[] children = sourceHandle.list();
        for(int i = 0; i < children.length; i++){
            FileHandle fileHandle = children[i];
            if(fileHandle.isDirectory()) {
                String roomName = fileHandle.name();
                if (!roomName.equals("key_icons")) {
                    createRoom(fileHandle);
                }
            }
        }
    }

    private void createLocation(FileHandle target){
        LocationNode locationNode = new LocationNode(target.name());
        locationNode.setSourcePath(target.toString() + "/");
        locationEditor.addLocation(locationNode);
    }

    private void createRoom(FileHandle room){
        Gdx.app.log("Loader", "  creating room: " + room.name());
        curRoom = locationEditor.createRoom(room.name(), room.parent().name());

        for(FileHandle roomChild : room.list()){
            if (roomChild.isDirectory()){
                createFrame(roomChild);
                for(FileHandle frameChild : roomChild.list()){
                    String extension = frameChild.extension();
                    if (extension.equals("xml")){
                        parseFrame(frameChild, room);
                    }

                    if (frameChild.isDirectory()){
                        parseZoom(frameChild, room);
                    }
                }
            }
        }
    }

    private void parseZoom(FileHandle zoomFolder, FileHandle roomFolder) {
        ZoomNode zoomNode = locationEditor.createZoom(roomFolder.name(), zoomFolder.name());

        for(FileHandle zoomChild : zoomFolder.list()){
            String extension = zoomChild.extension();
            if (extension.equals("xml")){
                try {
                    parseZoomXml(roomFolder, zoomNode, zoomChild);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void parseZoomXml(FileHandle roomFolder, ZoomNode zoomNode, FileHandle zoomXml) throws IOException {
        XmlReader reader = new XmlReader();
        XmlReader.Element root = reader.parse(zoomXml);
        NameParser namer = new NameParser();
        for(int i = 0; i < root.getChildCount(); ++i){
            XmlReader.Element child = root.getChild(i);

            String rawName = child.get("name");
            namer.parse(rawName);

            if (namer.isPrefixEquals("gem")){
                addGemToZoom(zoomNode, namer.getName(), namer.getPostfix(), child);
            }

            if (namer.isPrefixEquals("cover")){
                addCoverToZoom(zoomNode, rawName, child);
            }

            if (namer.isPrefixEquals("pack")){
                addPackToZoom(zoomNode, namer.getPostfix(), child);
            }

            if (namer.isPrefixEquals("coin")){
                addCoinToZoom(zoomNode, namer.getPostfix(), child);
            }
        }
    }

    private void addGemToZoom(ZoomNode zoomNode, String name, String postfix, XmlReader.Element data) {
        Gdx.app.log("Loader", "  addGemToZoom " + name);
        float x = data.getFloat("x");
        float y = data.getFloat("y");

        GemNode gemNode = new GemNode(name, Integer.parseInt(postfix), x, y);
        zoomNode.addGem(gemNode);
    }

    private void addCoverToZoom(ZoomNode zoomNode, String name, XmlReader.Element data) {
        float x = data.getFloat("x");
        float y = data.getFloat("y");

        CoverNode coverNode = new CoverNode(name, x, y);
        zoomNode.addCover(coverNode);
    }

    private void addPackToZoom(ZoomNode zoomNode, String variationString, XmlReader.Element data){
        int variation = Integer.parseInt(variationString);
        float x = data.getFloat("x");
        float y = data.getFloat("y");

        PackNode packNode = new PackNode(x, y);
        packNode.setVariation(variation);

        zoomNode.addPack(packNode);
    }

    private void addCoinToZoom(ZoomNode zoomNode, String variationString, XmlReader.Element data){
        int variation = Integer.parseInt(variationString);
        float x = data.getFloat("x");
        float y = data.getFloat("y");

        CoinNode coinNode = new CoinNode(x, y);
        coinNode.setVariation(variation);

        zoomNode.addCoin(coinNode);
    }

    private void createFrame(FileHandle fileHandle){
        //Gdx.app.log("Loader", "Creating frame for " + fileHandle.path());
        if(fileHandle.isDirectory()) {
            locationEditor.createFrame(Integer.parseInt(fileHandle.name()), fileHandle.parent().name());
        }
    }

    private void parseFrame(FileHandle xml, FileHandle room){
        FileHandle frameHandle = xml.parent();
        FileHandle roomHandle = frameHandle.parent();
        String roomName = roomHandle.name();
        int frameIndex = Integer.parseInt(frameHandle.name());

        XmlReader reader = new XmlReader();
        try {
            XmlReader.Element root = reader.parse(xml);
            NameParser namer = new NameParser();
            for(int i = 0; i < root.getChildCount(); i++){
                XmlReader.Element child = root.getChild(i);

                String rawName = child.get("name");
                namer.parse(rawName);

                if(namer.isPrefixEquals("pa")){
                    int targetFrameIndex = Integer.parseInt(namer.getPostfix());
                    createPassage(xml.parent(), namer.getName(), targetFrameIndex, child);
                }

                else if(namer.isPrefixEquals("gem")){
                    createGem(xml.parent(), namer.getName(), namer.getPostfix(), child);
                }

                else if(namer.isPrefixEquals("go")){
                    createGameObject(xml.parent(), namer.getName(), namer.getPostfix(), child);
                }

                else if(namer.isPrefixEquals("ob")){
                    createObstacle(namer.getName(), namer.getPostfix(), xml.parent(), child);
                }

                else  if(namer.isPrefixEquals("key")){
                    createKey(xml.parent(), namer.getName(), namer.getPostfix(), child);
                }

                else if(namer.isPrefixEquals("zt")){
                    createZoomTrigger(namer.getLongName(), xml.parent(), child);
                }

                else if(namer.isPrefixEquals("cover")){
                    createCover(rawName, xml.parent(), child);
                }

                else if(namer.isPrefixEquals("l")){
                    String status = namer.getPostfix();
                    boolean isAssembled = false;
                    if (status.equals("a")) {
                        isAssembled = true;
                    }
                    createLetter(roomName, frameIndex, namer.getName(1, 2), namer.getPostfix(1), isAssembled, child);
                }

                else if(namer.isPrefixEquals("pack")){
                    createPack(roomName, frameIndex, namer.getPostfix(), child);
                }

                else if (namer.isPrefixEquals("coin")){
                    createCoin(roomName, frameIndex, namer.getPostfix(), child);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createPassage(FileHandle parent, String name, int targetFrameIndex, XmlReader.Element element){
        //Gdx.app.log("Loader", "Creating passage " + name);
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        float w = element.getFloat("layerwidth");
        float h = element.getFloat("layerheight");
        locationEditor.createPassage(parent, name, targetFrameIndex, x, y, w, h);
    }

    private void createGameObject(FileHandle frame, String name, String variation, XmlReader.Element element){
        //Gdx.app.log("Loader", "      good " + name);
        String roomName = frame.parent().name();
        int frameIndex = Integer.parseInt(frame.name());
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        locationEditor.createGameObject(roomName, frameIndex, name, variation, x, y);
    }

    private void createGem(FileHandle frame, String name, String variation, XmlReader.Element element){
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        String roomName = frame.parent().name();
        int frameIndex = Integer.parseInt(frame.name());
        int variationInt = Integer.parseInt(variation);

        locationEditor.createGemInFrame(roomName, frameIndex, name, variationInt, x, y);
    }

    private void createObstacle(String name, String postfix, FileHandle frame, XmlReader.Element element){
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        locationEditor.createObstacle(name, postfix, frame.parent().name(), Integer.parseInt(frame.name()), x, y);
    }

    private void createKey(FileHandle frame, String name, String variation, XmlReader.Element element){
        String roomName = frame.parent().name();
        int frameIndex = Integer.parseInt(frame.name());
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        int variationInt = Integer.parseInt(variation);
        locationEditor.createKey(roomName, frameIndex, name, variationInt + "", x, y);
    }

    private void createZoomTrigger(String name, FileHandle frame, XmlReader.Element element){
        locationEditor.createZoomTrigger(name, frame.parent().name(), Integer.parseInt(frame.name()),
                element.getFloat("x"), element.getFloat("y"), element.getFloat("layerwidth"), element.getFloat("layerheight"));
    }

    private void createCover(String name, FileHandle frame, XmlReader.Element element){
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        String roomName = frame.parent().name();
        int frameIndex = Integer.parseInt(frame.name());

        locationEditor.createCover(roomName, frameIndex, name, x, y);
    }

    private void createLetter(String roomName, int frameIndex, String name, String variationString, boolean isAssembled, XmlReader.Element element) {
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        int variation = Integer.parseInt(variationString);

        locationEditor.createLetter(roomName, frameIndex, name, variation, isAssembled, x, y);
    }

    private void createPack(String roomName, int frameIndex,String variationString, XmlReader.Element element){
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        int variation = Integer.parseInt(variationString);

        locationEditor.createPack(roomName, frameIndex, variation, x, y);
    }

    private void createCoin(String roomName, int frameIndex, String variationString, XmlReader.Element element){
        float x = element.getFloat("x");
        float y = element.getFloat("y");
        int variation = Integer.parseInt(variationString);

        locationEditor.createCoin(roomName, frameIndex, variation, x, y);
    }
}
