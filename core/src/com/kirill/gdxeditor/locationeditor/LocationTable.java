package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextField;

/**
 * Created by Kirill on 14.10.2015.
 */
public class LocationTable extends VisTable {

    LocationNode locationNode;
    VisLabel locationNameLabel, firstRoomLabel;
    VisTextField locationNameField, firstRoomField;

    public LocationTable(final LocationNode locationNode){
        super();
        this.locationNode = locationNode;
        locationNameLabel = new VisLabel("Name");
        locationNameField = new VisTextField(locationNode.getLocationName());
        firstRoomLabel = new VisLabel("First Room");
        firstRoomField = new VisTextField(locationNode.getFirstRoomName());

        locationNameField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                locationNode.setLocationName(textField.getText());
            }
        });
        firstRoomField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                locationNode.setFirstRoomName(textField.getText());
            }
        });
        align(Align.topLeft);
        defaults().pad(10).space(10).expand();
        add(locationNameLabel);
        add(locationNameField).row();
        add(firstRoomLabel);
        add(firstRoomField);
    }
}
