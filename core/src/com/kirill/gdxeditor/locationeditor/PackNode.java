package com.kirill.gdxeditor.locationeditor;

public class PackNode extends FrameObjectNode {
    private boolean defaultVisible = true;

    public PackNode(float x, float y) {
        super("", x, y);
    }

    public void setDefaultInvisible() {
        defaultVisible = false;
    }

    public boolean isDefaultVisible() {
        return defaultVisible;
    }
}
