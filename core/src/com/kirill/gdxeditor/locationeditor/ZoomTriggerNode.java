package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 15.10.2015.
 */
public class ZoomTriggerNode extends Tree.Node {

    VisLabel nodeLabel;
    ClickZoneTable clickZoneTable;
    LocationEditor locationEditor;

    private boolean key, lighter;
    String ztName;
    public float posX, posY, w, h;

    public ZoomTriggerNode(final String name){
        super(new VisLabel("ZoomTrigger - " + name));
        ztName = name;
        locationEditor = Env.editor.getLocationEditor();
        nodeLabel = (VisLabel) getActor();
        clickZoneTable = new ClickZoneTable(this);
        nodeLabel.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                locationEditor.setComponentsWindowContent(clickZoneTable, "ZoomTrigger " + name);
            }
        });
    }

    public boolean isKey() {
        return key;
    }

    public void setKey(boolean key) {
        this.key = key;
    }

    public boolean isLighter() {
        return lighter;
    }

    public void setLighter(boolean lighter) {
        this.lighter = lighter;
    }

    public String getZtName(){
        return ztName;
    }
}
