package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisTable;

/**
 * Created by Kirill on 15.10.2015.
 */
public class ClickZoneTable extends VisTable {

    ZoomTriggerNode clickZoneNode;
    VisCheckBox keyCheckBox, lighterCheckBox;

    public ClickZoneTable(final ZoomTriggerNode clickZoneNode){
        super();
        this.clickZoneNode = clickZoneNode;
        keyCheckBox = new VisCheckBox("Key", clickZoneNode.isKey());
        lighterCheckBox = new VisCheckBox("Lighter", clickZoneNode.isLighter());
        keyCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                clickZoneNode.setKey(keyCheckBox.isChecked());
            }
        });
        lighterCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                clickZoneNode.setLighter(lighterCheckBox.isChecked());
            }
        });
        align(Align.topLeft);
        defaults().pad(5).space(5);
        add(keyCheckBox).left().row();
        add(lighterCheckBox).left().row();
    }
}
