package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.kotcrab.vis.ui.widget.VisLabel;

public class FrameObjectNode extends Tree.Node {
    private String name;
    private int variation;
    private float x;
    private float y;
    private float x1;
    private float y1;

    //region initialisation
    public FrameObjectNode(final String name, float x, float y) {
        super(new VisLabel(name));

        this.name = name;
        this.x = x;
        this.y = y;
    }

    public FrameObjectNode(final String name){
        super(new VisLabel(name));

        this.name = name;
    }

    public void setVariation(int variation) {
        this.variation = variation;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setX1(float x1) {
        this.x1 = x1;
    }

    public void setY1(float y1) {
        this.y1 = y1;
    }

    public void setPos(float x, float y){
        this.x = x;
        this.y = y;
    }

    public void setPos1(float x, float y){
        this.x1 = x;
        this.y1 = y;
    }
    //endregion

    //region getters
    public String getName() {
        return name;
    }

    public int getVariation() {
        return variation;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getX1() {
        return x1;
    }

    public float getY1() {
        return y1;
    }

    //endregion
}
