package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 16.10.2015.
 */
public class ObstacleNode extends Tree.Node {
    private VisLabel nodeLabel;
    private ObstacleTable obstacleTable;
    private LocationEditor locationEditor;
    public float posX, posY, openX, openY;

    private String name;
    private int type = 0;
    private String blocks;
    private boolean blocked;
    private boolean isOpened;
    private boolean hasOpenedTexture;
    private int crowbarLevel = 0;
    private Array<VariableObjectPointer> gemsToShow = new Array<VariableObjectPointer>();
    private Array<Integer> variationsToShow = new Array<Integer>();

    //region initialize
    public ObstacleNode(final String name){
        super(new VisLabel(name));
        this.name = name;
        locationEditor = Env.editor.getLocationEditor();
        nodeLabel = (VisLabel)getActor();
        obstacleTable = new ObstacleTable(this);
        nodeLabel.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                locationEditor.setComponentsWindowContent(obstacleTable, "Obstacle " + name);
            }
        });
    }

    public void setBlocks(String blocks) {
        this.blocks = blocks;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public void setHasOpenedTexture(boolean hasOpenedTexture) {
        this.hasOpenedTexture = hasOpenedTexture;
    }

    public void setKey(boolean key) {
        if (key) {
            type = 2;
        }
    }

    public void setCrowbar(boolean crowbar) {
        if (crowbar) {
            this.type = 1;
        }
    }

    public void setType(int type){
        this.type = type;
    }

    public void addGemToShow(String gemName, int variation){
        gemsToShow.add(new VariableObjectPointer(gemName, variation));
    }

    public void addVariationToShow(int variation){
        variationsToShow.add(variation);
    }
    //endregion


    //region getters
    public String getName(){
        return name;
    }

    public int getType() {
        return type;
    }

    public String getBlocks() {
        return blocks;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public boolean isHasOpenedTexture() {
        return hasOpenedTexture;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setIsOpened(boolean isOpened) {
        this.isOpened = isOpened;
    }

    public int getCrowbarLevel() {
        return crowbarLevel;
    }

    public void setCrowbarLevel(int crowbarLevel) {
        this.crowbarLevel = crowbarLevel;
    }

    @Deprecated
    public boolean hasGemsToShow(){
        if (gemsToShow.size == 0){
            return false;
        }else{
            return true;
        }
    }

    @Deprecated
    public Array<VariableObjectPointer> getGemsToShow() {
        return gemsToShow;
    }

    public boolean hasVariationsToShow(){
        if (variationsToShow.size == 0){
            return false;
        }else{
            return true;
        }
    }

    public Array<Integer> getVariationsToShow() {
        return variationsToShow;
    }

    //endregion

}
