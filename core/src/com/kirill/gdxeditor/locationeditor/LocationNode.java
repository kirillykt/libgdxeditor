package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxBuild;
import com.badlogic.gdx.utils.ObjectMap;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 14.10.2015.
 */
public class LocationNode extends Tree.Node {
    private String locationName = "", firstRoomName = "";
    VisLabel nodeLabel;
    LocationTable locationTable;
    LocationEditor locationEditor;
    private ObjectMap<String, RoomNode> rooms = new ObjectMap<String, RoomNode>();
    private String sourcePath;

    //
    private ObjectMap<String, ObjectMap<Integer, GemNode>> gems = new ObjectMap<String, ObjectMap<Integer, GemNode>>();
    private ObjectMap<String, KeyNode> keys = new ObjectMap<String, KeyNode>();

    private ObjectMap<Integer, Integer> variationSpecialWeights = new ObjectMap<Integer, Integer>();

    //region initialization
    public LocationNode(String text){
        super(new VisLabel("Location - " + text));
        locationName = text;
        locationEditor = Env.editor.getLocationEditor();
        nodeLabel = (VisLabel)getActor();
        locationTable = new LocationTable(this);
        nodeLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                locationEditor.setComponentsWindowContent(locationTable, "Location");
            }
        });
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
        nodeLabel.setText("Location - " + locationName);
    }

    public void setFirstRoomName(String firstRoomName) {
        this.firstRoomName = firstRoomName;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public void addVariationSpecialWeight(int index, int value){
        //Gdx.app.log("LocationNode.addVariationSpecialWeight", "index: " + index + ", value: " + value);
        variationSpecialWeights.put(index, value);
    }

    public void addRoom(RoomNode roomNode){
        this.add(roomNode);
        rooms.put(roomNode.getRoomName(), roomNode);
    }
    //endregion

    //region getters
    public String getLocationName() {
        return locationName;
    }

    public String getFirstRoomName() {
        return firstRoomName;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public ObjectMap<Integer, Integer> getVariationSpecialWeights() {
        return variationSpecialWeights;
    }

    public Array<RoomNode> getRooms(){
        Array<RoomNode> result = new Array<RoomNode>();
        for(ObjectMap.Entry<String, RoomNode> entry : rooms){
            result.add(entry.value);
        }

        return result;
    }

    public RoomNode getRoom(String name){
        return rooms.get(name);
    }

    public ObjectMap<String, ObjectMap<Integer, GemNode>> getGems() {
        return gems;
    }

    public ObjectMap<String, KeyNode> getKeys() {
        return keys;
    }

    //endregion

    //region logic
    public void search(){
        for(ObjectMap.Entry<String, RoomNode> entry : rooms){
            RoomNode roomNode = entry.value;
            for(FrameNode frameNode : roomNode.getFrames()){
                //gems
                for(GemNode gemNode : frameNode.getGemNodes()){
                    String name = gemNode.getName();
                    int variation = gemNode.getVariation();
                    if (gems.containsKey(name)){
                        gems.get(name).put(variation, gemNode);
                    }else{
                        ObjectMap<Integer, GemNode> pair = new ObjectMap<Integer, GemNode>();
                        pair.put(variation, gemNode);
                        gems.put(name, pair);
                    }
                }

                //keys
                for(KeyNode keyNode : frameNode.getKeyNodes()){
                    keys.put(keyNode.getName(), keyNode);
                }
            }
        }
    }
    //endregion
}
