package com.kirill.gdxeditor.locationeditor;

public class LetterNode extends FrameObjectNode {


    public LetterNode(final String name, int variation) {
        super(name);

        setVariation(variation);
    }
}
