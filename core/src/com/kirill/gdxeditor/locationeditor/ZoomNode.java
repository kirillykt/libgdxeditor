package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.widget.VisLabel;

public class ZoomNode extends Tree.Node {
    private String name;
    private Array<GemNode> gems = new Array<GemNode>();
    private Array<CoverNode> covers = new Array<CoverNode>();
    private Array<PackNode> packs = new Array<PackNode>();
    private Array<CoinNode> coins = new Array<CoinNode>();

    //region initialization
    public ZoomNode(String name) {
        super(new VisLabel("Zoom - " + name));

        this.name = name;
    }

    public void addGem(GemNode gemNode){
        this.add(gemNode);
        gems.add(gemNode);
    }

    public void addCover(CoverNode coverNode) {
        this.add(coverNode);
        covers.add(coverNode);
    }

    public void addPack(PackNode packNode){
        packs.add(packNode);
    }

    public void addCoin(CoinNode coinNode){
        coins.add(coinNode);
    }
    //endregion


    //region getters
    public String getName() {
        return name;
    }

    public Array<GemNode> getGems() {
        return gems;
    }

    public Array<CoverNode> getCovers() {
        return covers;
    }

    public Array<PackNode> getPacks() {
        return packs;
    }

    public Array<CoinNode> getCoins() {
        return coins;
    }

    //endregion
}
