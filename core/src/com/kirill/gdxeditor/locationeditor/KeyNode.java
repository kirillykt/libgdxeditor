package com.kirill.gdxeditor.locationeditor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.screens.LocationEditor;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 20.10.2015.
 */
public class KeyNode extends Tree.Node {
    private VisLabel keyLabel;
    private LocationEditor locationEditor;
    private KeyTable keyTable;
    private String keyName;
    public float x, y;
    private String variation;

    public KeyNode(final String keyName) {
        super(new VisLabel(keyName));
        this.keyName = keyName;
        keyLabel = (VisLabel)getActor();
        locationEditor = Env.editor.getLocationEditor();
        keyLabel.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                locationEditor.setComponentsWindowContent(keyTable, "Key " + keyName);
            }
        });
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public String getName() {
        return keyName;
    }

    public String getVariation() {
        return variation;
    }
}
