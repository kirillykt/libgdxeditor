package com.kirill.gdxeditor.locationeditor;

public class VariableObjectPointer {
    private String name;
    private int variation;

    //region initialization
    public VariableObjectPointer(String name, int variation){
        this.name = name;
        this.variation = variation;
    }
    //endregion

    //region getters
    public String getName() {
        return name;
    }

    public int getVariation() {
        return variation;
    }
    //endregion
}
