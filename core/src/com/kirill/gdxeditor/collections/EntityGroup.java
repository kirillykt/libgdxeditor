package com.kirill.gdxeditor.collections;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

public class EntityGroup {
	public Array<Entity> entitiesArray;
	public ObjectMap<Long, Entity> entitiesMap;
	
	public EntityGroup(){
		
	}
}
