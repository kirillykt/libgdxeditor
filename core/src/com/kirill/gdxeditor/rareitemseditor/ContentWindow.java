package com.kirill.gdxeditor.rareitemseditor;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.*;

/**
 * Created by Kirill on 07.12.2015.
 */
public class ContentWindow extends VisWindow {

    float W = 1280f, H = 720f;

    VisScrollPane scrollPane;
    VisTable table;

    RareItemListLine listLine;

    VisTextField itemNameField, idField, locationField, fullNameField;
    VisLabel itemNameLabel, idLabel, locationLabel, fullNameLabel;
    VisTextField priceField, restoPriceField, assPriceField, restoValField, assValField;
    VisLabel priceLabel, restoPriceLabel, assPriceLabel, restoValLabel, assValLabel;


    VisLabel descriptionAreaLabel, vanishPartRestLabel, vanishPartAssLabel, newPartRestLabel, newPartAssLabel;
    VisTextArea descriptionArea, vanishPartRestArea, vanishPartAssArea, newPartRestArea, newPartAssArea;

    VisLabel qualityLabel;
    VisSelectBox<String> qualitySelectBox;
    VisSelectBox<String> shopPlaceBox;

    VisCheckBox historicalValueCheckBox, idealStateCheckBox, artValueCheckBox, rarityCheckBox, jewelryCheckBox, antiqueCheckBox;


    public ContentWindow(String title) {
        super(title);
        table = new VisTable();
        table.defaults().left().top().pad(4).expandX();
        table.setSize(getWidth() - 50, getHeight() - 50);
        scrollPane = new VisScrollPane(table);
        addActor(scrollPane);
        setSize(500, 600);
        setPosition(W - getWidth(), H / 2 - getHeight() / 2);
        scrollPane.setSize(getWidth(), getHeight() - getTitleLabel().getPrefHeight());
        createActors();
        createListeners();
    }

    void createActors(){
        itemNameField = new VisTextField();
        fullNameField = new VisTextField();
        idField = new VisTextField();

        locationField = new VisTextField("location");

        /*priceField = new NumberSelector("Price", 0, -999999999, 99999999);
        restoPriceField = new NumberSelector("Rest Price", 0, -9999999, 999999999);
        restoValField = new NumberSelector("Rest Value", 0, -999999999, 999999999);
        assPriceField = new NumberSelector("Ass Price", 0, -999999999, 999999999);
        assValField = new NumberSelector("Ass Value", 0, -999999999, 999999999);*/
        priceField = new VisTextField();
        restoPriceField = new VisTextField();
        restoValField = new VisTextField();
        assPriceField = new VisTextField();
        assValField = new VisTextField();

        priceLabel = new VisLabel("Price");
        restoPriceLabel = new VisLabel("Rest Price");
        restoValLabel = new VisLabel("Rest Value");
        assPriceLabel = new VisLabel("Ass Price");
        assValLabel = new VisLabel("Ass Value");

        qualitySelectBox = new VisSelectBox<String>();
        String[] qualities = new String[]{"Good", "Bad"};
        qualitySelectBox.setItems(qualities);

        String[] shopPlaces = new String[]{"center", "top", "left", "center_center", "center_right", "top_left", "top_center", "top_right"};
        shopPlaceBox = new VisSelectBox<String>();
        shopPlaceBox.setItems(shopPlaces);


        itemNameLabel = new VisLabel("Name");
        fullNameLabel = new VisLabel("Full Name");
        idLabel = new VisLabel("ID");
        locationLabel = new VisLabel("Location");
        qualityLabel = new VisLabel("Quality");


        descriptionAreaLabel = new VisLabel("Description");
        descriptionArea = new VisTextArea("");
        vanishPartRestLabel = new VisLabel("Vanish Rest");
        vanishPartRestArea = new VisTextArea("");
        vanishPartAssLabel = new VisLabel("Vanish Ass");
        vanishPartAssArea = new VisTextArea("");

        newPartRestLabel = new VisLabel("New Rest");
        newPartRestArea = new VisTextArea("");
        newPartAssLabel = new VisLabel("New Ass");
        newPartAssArea = new VisTextArea("");

        historicalValueCheckBox = new VisCheckBox("Historical");
        idealStateCheckBox = new VisCheckBox("Ideal State");
        artValueCheckBox = new VisCheckBox("Art Value");
        rarityCheckBox = new VisCheckBox("Rarity");
        jewelryCheckBox = new VisCheckBox("Jewelry");
        antiqueCheckBox = new VisCheckBox("Antique");


        table.add(itemNameLabel);
        table.add(itemNameField).row();
        table.add(fullNameLabel);
        table.add(fullNameField).row();
        table.add(idLabel);
        table.add(idField).row();
        table.add(locationLabel);
        table.add(locationField).row();
        table.add(qualityLabel);
        table.add(qualitySelectBox).row();

        VisLabel shopPlace = new VisLabel("Shop Place");
        table.add(shopPlace);
        table.add(shopPlaceBox).row();

        table.add(priceLabel);
        table.add(priceField).row();
        table.add(restoPriceLabel);
        table.add(restoPriceField).row();
        table.add(restoValLabel);
        table.add(restoValField).row();
        table.add(assPriceLabel);
        table.add(assPriceField).row();
        table.add(assValLabel);
        table.add(assValField).row();

        table.add(descriptionAreaLabel).row();
        table.add(descriptionArea).colspan(2).width(getWidth() - 50).height(300).expandX().row();

        table.add(vanishPartRestLabel).row();
        table.add(vanishPartRestArea).colspan(2).width(getWidth() - 50).height(150).expandX().row();

        table.add(vanishPartAssLabel).row();
        table.add(vanishPartAssArea).colspan(2).width(getWidth() - 50).height(150).expandX().row();

        table.add(newPartRestLabel).row();
        table.add(newPartRestArea).colspan(2).width(getWidth() - 50).height(150).expandX().row();

        table.add(newPartAssLabel).row();
        table.add(newPartAssArea).colspan(2).width(getWidth() - 50).height(150).expandX().row();
        table.add(historicalValueCheckBox).row();
        table.add(idealStateCheckBox).row();
        table.add(artValueCheckBox).row();
        table.add(rarityCheckBox).row();
        table.add(jewelryCheckBox).row();
        table.add(antiqueCheckBox).row();
    }

    void createListeners(){
        priceField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null || !isInteger(priceField.getText())){
                    return;
                }
                listLine.price = Integer.parseInt(priceField.getText());
            }
        });

        itemNameField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.setName(itemNameField.getText());
            }
        });

        idField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.setItemID(textField.getText());
            }
        });

        locationField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.location = textField.getText();
            }
        });

        qualitySelectBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null) return;
                listLine.quality = qualitySelectBox.getSelected();
            }
        });

        shopPlaceBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null) return;
                listLine.shopPlace = shopPlaceBox.getSelected();
            }
        });

        fullNameField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.fullName = textField.getText();
            }
        });

        restoPriceField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if (listLine == null || !isInteger(restoPriceField.getText())) return;
                listLine.restPrice = Integer.parseInt(restoPriceField.getText());
            }
        });

        restoValField.setTextFieldListener(new VisTextField.TextFieldListener(){
        @Override
        public void keyTyped(VisTextField textField, char c) {
                if(listLine == null || !isInteger(restoValField.getText())) return;
                listLine.restVal = Integer.parseInt(restoValField.getText());
            }
        });

        assPriceField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if (listLine == null || !isInteger(assPriceField.getText())) return;
                listLine.assPrice = Integer.parseInt(assPriceField.getText());
            }
        });

        assValField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if (listLine == null || !isInteger(assValField.getText())) return;
                listLine.assVal = Integer.parseInt(assValField.getText());
            }
        });

        descriptionArea.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.description = textField.getText();
            }
        });

        vanishPartRestArea.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.vanishPartRest = textField.getText();
            }
        });

        vanishPartAssArea.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.vanishPartAss = textField.getText();
            }
        });

        fullNameField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                if(listLine == null) return;
                listLine.fullName = textField.getText();
            }
        });

        historicalValueCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null)return;
                listLine.historical = historicalValueCheckBox.isChecked();
            }
        });

        idealStateCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null)return;
                listLine.ideal = idealStateCheckBox.isChecked();
            }
        });

        artValueCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null)return;
                listLine.art = artValueCheckBox.isChecked();
            }
        });

        rarityCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null)return;
                listLine.rarity = rarityCheckBox.isChecked();
            }
        });

        jewelryCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null)return;
                listLine.jewelry = jewelryCheckBox.isChecked();
            }
        });

        antiqueCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(listLine == null)return;
                listLine.antique = antiqueCheckBox.isChecked();
            }
        });
    }

    public static boolean isInteger(String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(!Character.isDigit(s.charAt(i))) return false;
        }
        return true;
    }


    public void onRareItemListLineSelected(RareItemListLine rareItemListLine){
        getTitleLabel().setText("Content - " + rareItemListLine.getName() + " " + rareItemListLine.getItemID());
        saveCurrentLine();
        listLine = rareItemListLine;
        itemNameField.setText(rareItemListLine.getName());
        idField.setText(rareItemListLine.getItemID());
        locationField.setText(rareItemListLine.location);
        qualitySelectBox.setSelected(rareItemListLine.quality);
        priceField.setText(rareItemListLine.price + "");
        restoPriceField.setText(rareItemListLine.restPrice + "");
        restoValField.setText(rareItemListLine.restVal + "");
        assPriceField.setText(rareItemListLine.assPrice + "");
        assValField.setText(rareItemListLine.assVal + "");
        fullNameField.setText(rareItemListLine.fullName);
        descriptionArea.setText(rareItemListLine.description);
        vanishPartRestArea.setText(rareItemListLine.vanishPartRest);
        vanishPartAssArea.setText(rareItemListLine.vanishPartAss);
        newPartAssArea.setText(rareItemListLine.newPartAss);
        newPartRestArea.setText(rareItemListLine.newPartRest);
        shopPlaceBox.setSelected(rareItemListLine.shopPlace);
        historicalValueCheckBox.setChecked(rareItemListLine.historical);
        idealStateCheckBox.setChecked(rareItemListLine.ideal);
        rarityCheckBox.setChecked(rareItemListLine.rarity);
        artValueCheckBox.setChecked(rareItemListLine.art);
        jewelryCheckBox.setChecked(rareItemListLine.jewelry);
        antiqueCheckBox.setChecked(rareItemListLine.antique);
    }

    private void saveCurrentLine(){
        if(listLine == null) return;
        listLine.setName(itemNameField.getText());
        listLine.setItemID(idField.getText());
        listLine.location = locationField.getText();
        listLine.quality = qualitySelectBox.getSelected();
        listLine.price = Integer.parseInt(priceField.getText());
        listLine.restPrice = Integer.parseInt(restoPriceField.getText());
        listLine.restVal = Integer.parseInt(restoValField.getText());
        listLine.assPrice = Integer.parseInt(assPriceField.getText());
        listLine.assVal = Integer.parseInt(assValField.getText());
        listLine.fullName = fullNameField.getText();
        listLine.description = descriptionArea.getText();
        listLine.vanishPartRest = vanishPartRestArea.getText();
        listLine.vanishPartAss = vanishPartAssArea.getText();
        listLine.newPartRest = newPartRestArea.getText();
        listLine.newPartAss = newPartAssArea.getText();
        listLine.shopPlace = shopPlaceBox.getSelected();
        listLine.historical = historicalValueCheckBox.isChecked();
        listLine.ideal = idealStateCheckBox.isChecked();
        listLine.rarity = rarityCheckBox.isChecked();
        listLine.art = artValueCheckBox.isChecked();
        listLine.jewelry = jewelryCheckBox.isChecked();
        listLine.antique = antiqueCheckBox.isChecked();
    }

}
