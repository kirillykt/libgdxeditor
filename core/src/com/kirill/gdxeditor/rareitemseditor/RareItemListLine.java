package com.kirill.gdxeditor.rareitemseditor;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisList;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextField;

/**
 * Created by Kirill on 16.11.2015.
 */
public class RareItemListLine extends VisTable {

    VisLabel nameField, idField;
    private String itemID = "";
    public String location = "", fullName = "", description = "", vanishPartRest = "", vanishPartAss = "", newPartRest = "", newPartAss = "", quality = "Good", shopPlace = "center";
    RareItemsSelectTable selectTable;
    public int price, restPrice, assPrice, restVal, assVal;
    public boolean historical, antique, jewelry, rarity, ideal, art;

    public RareItemListLine(RareItemsSelectTable table){
        super();
        selectTable = table;
        nameField = new VisLabel("NAME");
        setName("NAME");
        idField = new VisLabel("ID");
        itemID = "ID";
        defaults().pad(4).space(4);
        add(nameField).expandX();
        add(idField).expandX();
        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                selectTable.onListLineSelect(RareItemListLine.this);
            }
        });
    }

    public void reload(){
        nameField.setText(getName());
        idField.setText(itemID);
    }

    public void setItemID(String id){
        itemID = id;
        idField.setText(id);
    }

    public String getItemID(){
        return itemID;
    }

    @Override
    public void setName(String text){
        super.setName(text);
        nameField.setText(text);
    }

    public void onSelect(){
        setBackground("default-select-selection");
    }

    public void onDeselect(){
        setBackground("window-bg");
    }

}
