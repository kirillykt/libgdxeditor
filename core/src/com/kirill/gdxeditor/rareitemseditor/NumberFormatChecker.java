package com.kirill.gdxeditor.rareitemseditor;

/**
 * Created by Kirill on 07.12.2015.
 */
public class NumberFormatChecker {
    public static boolean isInteger(String str){
        try{
            int num = Integer.parseInt(str);
        } catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public static boolean isDigit(char c){
        if(c == '.') return true;
        return Character.isDigit(c);
    }
}
