package com.kirill.gdxeditor.rareitemseditor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisWindow;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Kirill on 16.11.2015.
 */
public class RareItemsSelectTable extends VisWindow {

    Array<RareItemListLine> lines;
    VisTable table;
    VisScrollPane scrollPane;
    RareItemListLine selectedLine;
    ContentWindow contentWindow;

    float W = 1280f, H = 720f;
    float inner_pad = 50f;

    FileHandle saveFile;
    String savePath = "save.xml";

    public RareItemsSelectTable(){
        super("Items");
        saveFile = Gdx.files.local(savePath);

        table = new VisTable();
        table.defaults().left().pad(4);
        lines = new Array<RareItemListLine>();
        scrollPane = new VisScrollPane(table);
        setSize(550, H / 2 + 200);
        scrollPane.setY(25);
        addActor(scrollPane);
        VisTextButton addButton = new VisTextButton("Add");
        addActor(addButton);
        addButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                addNewListLine();
            }
        });

        VisTextButton removeButton = new VisTextButton("Remove");
        removeButton.setX(addButton.getWidth());
        removeButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                removeSelectedLine();
                save();
            }
        });
        addActor(removeButton);

        VisTextButton saveButton = new VisTextButton("Save");
        saveButton.setX(removeButton.getX() + removeButton.getWidth());
        saveButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                save();
            }
        });
        addActor(saveButton);
        load();
    }

    void removeSelectedLine(){
        lines.removeValue(selectedLine, true);
        table.removeActor(selectedLine);
        table.reset();
        for (int i = 0; i < lines.size; i++) {
            table.add(lines.get(i)).width(520).expandX().row();
        }
        selectedLine = null;

    }

    public void addNewListLine(){
        RareItemListLine rareItemListLine = new RareItemListLine(this);
        table.add(rareItemListLine).width(520).expandX().row();
        lines.add(rareItemListLine);
    }

    public void setContentWindow(ContentWindow contentWindow){
        this.contentWindow = contentWindow;
    }

    public void onListLineSelect(RareItemListLine listLine){
        if(selectedLine != null){
            selectedLine.onDeselect();
        }
        listLine.onSelect();
        selectedLine = listLine;
        contentWindow.onRareItemListLineSelected(selectedLine);
    }

    @Override
    public void setSize(float width, float height){
        super.setSize(width, height);
        table.setSize(width - inner_pad, height - inner_pad);
        scrollPane.setSize(width - inner_pad, height - inner_pad);
    }

    String name = "name", id = "id", quality = "quality", location = "location", price = "price", restPrice = "restPrice", restVal = "restVal", description = "description";
    String assPrice = "assPrice", assVal = "assVal", fullName = "fullName", vanishPartRest = "vanishPartRest", vanishPartAss = "vanishPartAss", newPartRest = "newPartRest", newPartAss = "newPartAss";
    String shopPlace = "shopPlace";
    String historical = "historical", ideal = "ideal", rarity = "rarity", art = "art", jewelry = "jewelry", antique = "antique";

    void load(){
        if(!saveFile.exists()) return;
        XmlReader xmlReader = new XmlReader();
        try {
            XmlReader.Element root = xmlReader.parse(saveFile);
            for (int i = 0; i < root.getChildCount(); i++) {
                XmlReader.Element child = root.getChild(i);
                RareItemListLine listLine = new RareItemListLine(this);
                listLine.setName(child.get(name, ""));
                listLine.setItemID(child.get(id, ""));
                listLine.location = child.get(location, "");
                listLine.quality = child.get(quality, "Good");
                listLine.price = child.getInt(price, 0);
                listLine.restPrice = child.getInt(restPrice, 0);
                listLine.restVal = child.getInt(restVal, 0);
                listLine.assPrice = child.getInt(assPrice, 0);
                listLine.assVal = child.getInt(assVal, 0);
                listLine.fullName = child.get(fullName, "");
                listLine.description = child.get(description, "");
                listLine.vanishPartRest = child.get(vanishPartRest, "");
                listLine.vanishPartAss = child.get(vanishPartAss, "");
                listLine.newPartRest = child.get(newPartRest, "");
                listLine.newPartAss = child.get(newPartAss, "");
                listLine.shopPlace = child.get(shopPlace, "");
                listLine.historical = child.getBoolean(historical, false);
                listLine.ideal = child.getBoolean(ideal, false);
                listLine.rarity = child.getBoolean(rarity, false);
                listLine.art = child.getBoolean(art, false);
                listLine.jewelry = child.getBoolean(jewelry, false);
                listLine.antique = child.getBoolean(antique, false);
                lines.add(listLine);
                table.add(listLine).width(520).expandX().row();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("root");
            document.appendChild(root);
            for (int i = 0; i < lines.size; i++) {
                RareItemListLine listLine = lines.get(i);
                Element itemElement = document.createElement("Item");
                itemElement.setAttribute(name, listLine.getName());
                itemElement.setAttribute(id, listLine.getItemID());
                itemElement.setAttribute(quality, listLine.quality);
                itemElement.setAttribute(location, listLine.location);
                itemElement.setAttribute(price, listLine.price + "");
                itemElement.setAttribute(restPrice, listLine.restPrice + "");
                itemElement.setAttribute(restVal, listLine.restVal + "");
                itemElement.setAttribute(assPrice, listLine.assPrice + "");
                itemElement.setAttribute(assVal, listLine.assVal + "");
                itemElement.setAttribute(fullName, listLine.fullName);
                itemElement.setAttribute(description, listLine.description);
                itemElement.setAttribute(vanishPartRest, listLine.vanishPartRest + "");
                itemElement.setAttribute(vanishPartAss, listLine.vanishPartAss + "");
                itemElement.setAttribute(newPartRest, listLine.newPartRest + "");
                itemElement.setAttribute(newPartAss, listLine.newPartAss + "");
                itemElement.setAttribute(shopPlace, listLine.shopPlace);
                itemElement.setAttribute(historical, listLine.historical + "");
                itemElement.setAttribute(ideal, listLine.ideal + "");
                itemElement.setAttribute(rarity, listLine.rarity + "");
                itemElement.setAttribute(art, listLine.art + "");
                itemElement.setAttribute(jewelry, listLine.jewelry + "");
                itemElement.setAttribute(antique, listLine.antique + "");

                root.appendChild(itemElement);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource domSource = new DOMSource(document);

            if(!saveFile.exists()){
                saveFile.file().createNewFile();
            }
            saveBackup(saveFile);
            /*if(!backupFile.exists())
                backupFile.file().createNewFile();*/
            /*StreamResult result = new StreamResult(backupFile.file());
            transformer.transform(domSource, result);*/
            StreamResult result = new StreamResult(saveFile.file());
            transformer.transform(domSource, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (TransformerConfigurationException e){
            e.printStackTrace();
        }
        catch (TransformerException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    void saveBackup(FileHandle file){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy-hh-mm-ss");
        FileHandle backupFile = Gdx.files.local("backup" + format.format(date) + ".xml");
        file.copyTo(backupFile);
    }


}
