package com.kirill.gdxeditor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Kirill on 22.10.2015.
 */
public class HintShader implements Screen {

    OrthographicCamera camera;
    SpriteBatch batch;
    Sprite background;
    Texture mask;
    ShaderProgram shader;
    FrameBuffer buffer;
    TextureRegion bufferRegion;
    ParticleEffect effect;

    float w = 2048f, h = 1536f;
    Sprite sprite;
    @Override
    public void show() {
        camera = new OrthographicCamera(w, h);
        background = new Sprite(new Texture("leftBg.jpg"));
        background.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        background.getTexture().setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        batch = new SpriteBatch();
        background.setPosition(0, 0);
        camera.setToOrtho(false, w, h);
        batch.setProjectionMatrix(camera.combined);
        ShaderProgram.pedantic = false;
        shader = new ShaderProgram(Gdx.files.internal("shaders/noise.vert"), Gdx.files.internal("shaders/noise.frag"));
        shader.begin();
        shader.setUniformf("rt_w", w);
        shader.setUniformf("rt_h", h);
        shader.setUniformf("vx_offset", 10);
        shader.end();
        if(!shader.isCompiled()){
            Gdx.app.error("SHADERS", shader.getLog());
        }
        buffer = new FrameBuffer(Pixmap.Format.RGBA8888, (int)w, (int)h, false);

        bufferRegion = new TextureRegion(buffer.getColorBufferTexture());
        bufferRegion.flip(false, false);

        sprite = new Sprite();
        effect = new ParticleEffect();
        effect.load(Gdx.files.internal("particles/ring.p"), Gdx.files.internal("particles/"));



        Gdx.gl.glClearColor(0, 0, 0, 1);
    }

    float time = 0;
    float shaderTime = 0;

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        shaderTime += delta * 5f;
        effect.update(delta);
        //batch.setShader(shader);
        changeLensSize(delta);
        shader.begin();
        shader.setUniformf("resolution", new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        //shader.setUniformf("resolution", new Vector2(w, h));
        shader.setUniformf("time", shaderTime);
        shader.setUniformf("lensSize", lensSize);
        shader.setUniformf("mouse", new Vector2(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY()));
        shader.end();
        batch.setShader(shader);
        batch.begin();
        background.draw(batch);
        effect.setPosition(500, 500);
        effect.draw(batch);
        bufferRegion.setTexture(buffer.getColorBufferTexture());
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            effect.start();
        }
        batch.end();
    }

    float lensSize = 0.2f, stepValue = 0.05f;
    float lensStep = stepValue;

    void changeLensSize(float delta){
        lensSize += delta * lensStep;
        if(lensSize > 0.2f){
            lensStep = -stepValue;
        }
        if(lensSize <= 0.1f){
            lensStep = stepValue;
        }

    }



    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        background.getTexture().dispose();

    }
}
