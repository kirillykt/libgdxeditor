package com.kirill.gdxeditor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kirill.gdxeditor.rareitemseditor.ContentWindow;
import com.kirill.gdxeditor.rareitemseditor.RareItemListLine;
import com.kirill.gdxeditor.rareitemseditor.RareItemsSelectTable;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.*;

/**
 * Created by Kirill on 16.11.2015.
 */
public class RareItemsCreator implements Screen {

    float W = 1280f, H = 720f;

    Stage stage;
    FitViewport viewport;
    RareItemsSelectTable selectTable;
    OrthographicCamera camera;
    ContentWindow contentWindow;

    @Override
    public void show() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(W, H, camera);
        stage = new Stage(viewport);
        selectTable = new RareItemsSelectTable();
        contentWindow = new ContentWindow("Content");
        selectTable.setContentWindow(contentWindow);

        stage.addActor(selectTable);
        stage.addActor(contentWindow);
        Gdx.input.setInputProcessor(stage);
        Gdx.gl.glClearColor(0, 0, 0, 1);
    }

    float elapsedTime = 0, saveTime = 60f;
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

        elapsedTime += delta;
        if(elapsedTime > saveTime){
            selectTable.save();
            elapsedTime = 0;
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        selectTable.save();
        stage.dispose();
        VisUI.dispose();
    }
}
