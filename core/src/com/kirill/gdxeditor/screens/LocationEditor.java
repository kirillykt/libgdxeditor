package com.kirill.gdxeditor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kirill.gdxeditor.locationeditor.*;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Kirill on 13.10.2015.
 */
public class LocationEditor implements Screen {
    private String[] args;

    private static final String TAG_VITYA = "Витя виноват";

    Stage stage;
    FitViewport viewport;
    OrthographicCamera camera;
    float w = 1280f, h = 720f;
    SpriteBatch batch;
    Table root;
    VisTree tree;
    VisLabel testLabel;
    VisScrollPane scrollPane;
    VisWindow componentsWindow;
    FileChooser fileChooser;

    private ObjectMap<String, LocationNode> locations = new ObjectMap<String, LocationNode>();
    private ObjectMap<String, RoomNode> rooms = new ObjectMap<String, RoomNode>();
    private ObjectMap<String, IntMap <FrameNode>> frames = new ObjectMap<String, IntMap<FrameNode>>();

    private ObjectMap<String, ObstacleNode> obstacles = new ObjectMap<String, ObstacleNode>();
    private ObjectMap<String, LetterNode> letters = new ObjectMap<String, LetterNode>();

    private LocationNode currentLocation = null;

    Preferences preferences;

    Loader loader;
    FileHandle saveFile;

    public LocationEditor(String[] args){
        this.args = args;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(w, h);
        batch = new SpriteBatch();
        viewport = new FitViewport(w, h, camera);
        stage = new Stage(viewport, batch);

        loader = Loader.getInstance();
        preferences = Gdx.app.getPreferences("com.kirillka.gdxeditor");
        createActors();
        Gdx.input.setInputProcessor(stage);

        if (args.length > 0){
            FileHandle fileHandle = new FileHandle(args[0]);
            loader.load(fileHandle);
            postLoad(fileHandle);
            save();
        }
    }

    void createActors(){
        tree = new VisTree();
        testLabel = new VisLabel("sample text");

        FileChooser.setFavoritesPrefsName("LocationEditorPrefs");
        fileChooser = new FileChooser(FileChooser.Mode.OPEN);
        fileChooser.setSelectionMode(FileChooser.SelectionMode.DIRECTORIES);
        fileChooser.setMultiselectionEnabled(false);


        tree.setSize(500, 500);

        scrollPane = new VisScrollPane(tree);
        scrollPane.setSize(300, h);
        scrollPane.setPosition(0, h - scrollPane.getHeight());

        componentsWindow = new VisWindow("");
        componentsWindow.setSize(400, h / 2);
        componentsWindow.setPosition(w - componentsWindow.getWidth() - 50f, h - componentsWindow.getHeight() - 200);

        stage.addActor(scrollPane);
        stage.addActor(componentsWindow);
        VisTextButton addBtn = new VisTextButton("add");
        addBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.addActor(fileChooser.fadeIn());
                fileChooser.setListener(new FileChooserAdapter(){
                    @Override
                    public void selected(FileHandle file) {
                        loader.load(file);
                        if(!preferences.getBoolean(file.path(), false)) {
                            Gdx.app.log("PREFERENCES", file.path() + " contains = " + !preferences.getBoolean(file.path(), false));
                            fileChooser.addFavorite(file);
                            preferences.putBoolean(file.path(), true);
                            preferences.flush();
                        }
                        postLoad(file);
                    }
                });
            }
        });
        VisTextButton saveBtn = new VisTextButton("save");
        saveBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                save();
            }
        });
        saveBtn.setPosition(addBtn.getX() + addBtn.getWidth() + 10, 0);
        stage.setScrollFocus(scrollPane);
        stage.addActor(addBtn);
        stage.addActor(saveBtn);
    }

    public void addLocation(LocationNode node){
        String name = node.getLocationName();
        locations.put(name, node);
        tree.add(node);

        currentLocation = node;
    }

    public RoomNode createRoom(String name, String locationName){
        RoomNode roomNode = new RoomNode(name);

        rooms.put(name, roomNode);
        frames.put(name, new IntMap<FrameNode>());
        locations.get(locationName).addRoom(roomNode);

        return roomNode;
    }

    public void createFrame(int index, String roomName){
        FrameNode node = new FrameNode(index);
        frames.get(roomName).put(index, node);
        rooms.get(roomName).addFrame(node);
    }

    public void createPassage(FileHandle frame, String name, int targetFrameIndex, float x, float y, float w, float h){
        PassageNode passageNode = new PassageNode(name, targetFrameIndex);
        int sourceTargetIndex = Integer.parseInt(frame.name());
        FrameNode frameNode = frames.get(frame.parent().name()).get(sourceTargetIndex);
        frameNode.addPassage(passageNode);
        passageNode.posX = x;
        passageNode.posY = y;
        passageNode.w = w;
        passageNode.h = h;
    }

    public void createGameObject(String roomName, int frameIndex, String name, String variation, float x, float y){
        GameObjectNode goodNode = new GameObjectNode(name);
        FrameNode frameNode = frames.get(roomName).get(frameIndex);
        frameNode.addGood(goodNode);
        goodNode.posX = x;
        goodNode.posY = y;
        goodNode.variation = variation;
    }

    public void createGemInFrame(String roomName, int frameIndex, String name, int variation, float x, float y) {
        GemNode gemNode = new GemNode(name, variation, x, y);
        frames.get(roomName).get(frameIndex).addGem(gemNode);
    }

    public void createKey(String roomName, int frameIndex, String name, String variation, float x, float y){
        KeyNode keyNode = new KeyNode(name);
        frames.get(roomName).get(frameIndex).addKey(keyNode);
        keyNode.x = x;
        keyNode.y = y;
        keyNode.setVariation(variation);
    }

    public void createObstacle(String name, String postfix, String roomName, int frameIndex, float x, float y){
        ObstacleNode node;
        //Gdx.app.log("LocationEditor", "createObstacle(" + name);
        if (obstacles.containsKey(name)){
            node = obstacles.get(name);
        }else{
            node = new ObstacleNode(name);

            frames.get(roomName).get(frameIndex).addObstacle(node);
            obstacles.put(name, node);
        }

        if (postfix.equals("o")){
            node.setHasOpenedTexture(true);
            node.openX = x;
            node.openY = y;
        }else {
            node.posX = x;
            node.posY = y;
        }
    }

    public void createZoomTrigger(String name, String roomName, int frameIndex, float x, float y, float w, float h){
        RoomNode roomNode = currentLocation.getRoom(roomName);

        ZoomTriggerNode node = new ZoomTriggerNode(name);
        node.posX = x;
        node.posY = y;
        node.h = h;
        node.w = w;
        frames.get(roomName).get(frameIndex).addZoomTrigger(node);
    }

    public void createCover(String roomName, int frameIndex, String name, float x, float y) {
        CoverNode coverNode = new CoverNode(name, x, y);
        frames.get(roomName).get(frameIndex).addCover(coverNode);
    }

    public void createLetter(String roomName, int frameIndex, String name, int variation, boolean isAssembled, float x, float y) {
        StringBuilder addressBuilder = new StringBuilder(roomName);
        addressBuilder.append("_");
        addressBuilder.append(frameIndex);
        addressBuilder.append("_");
        addressBuilder.append(name);
        addressBuilder.append("_");
        addressBuilder.append(variation);
        String address = addressBuilder.toString();


        LetterNode letterNode;
        if (letters.containsKey(address)){
            letterNode = letters.get(address);
        }else {
            letterNode = new LetterNode(name, variation);
            letters.put(address, letterNode);
            frames.get(roomName).get(frameIndex).addLetter(letterNode);
        }

        if (isAssembled){
            letterNode.setPos1(x, y);
        }else{
            letterNode.setPos(x, y);
        }
    }

    public void createPack(String roomName, int frameIndex, int variation, float x, float y){
        PackNode packNode = new PackNode(x, y);
        packNode.setVariation(variation);
        frames.get(roomName).get(frameIndex).addPack(packNode);
    }

    public void createCoin(String roomName, int frameIndex, int variation, float x, float y){
        CoinNode coinNode = new CoinNode(x, y);
        coinNode.setVariation(variation);
        frames.get(roomName).get(frameIndex).addCoin(coinNode);
    }

    private void postLoad(FileHandle file) {
        Iterator<LocationNode> locationNodeIterator = locations.values().iterator();
        LocationNode locationNode = locationNodeIterator.next();

        //read meta
        FileHandle metaFile = file.child("meta.xml");
        if (metaFile.exists()) {
            XmlReader reader = new XmlReader();
            try {
                XmlReader.Element locationElement = reader.parse(metaFile);
                parseMeta(locationNode, locationElement);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            Gdx.app.log("LOG", "No meta file");
        }

        locationNode.search();

        //obstacles by keys
        for(RoomNode roomNode : locationNode.getRooms()) {
            for (FrameNode frameNode : roomNode.getFrames()) {
                for(ObjectMap.Entry<String, ObstacleNode> entry : frameNode.getObstacleNodes()){
                    String name = entry.key;
                    ObstacleNode obstacleNode = entry.value;

                    //gems
                    if (obstacleNode.hasGemsToShow()){
                        for(VariableObjectPointer variableObjectPointer : obstacleNode.getGemsToShow()){
                            String gemName = variableObjectPointer.getName();
                            int gemVariation = variableObjectPointer.getVariation();
                            ObjectMap<Integer, GemNode> variationsOfGem = locationNode.getGems().get(gemName);
                            //Gdx.app.log("LocationEditor", "gem: " + gemName + " variation: " + gemVariation);
                            GemNode gemNode = variationsOfGem.get(gemVariation);
                            gemNode.setDefaultInvisible();
                        }
                    }

                    if (obstacleNode.hasVariationsToShow()){
                        for(Integer variationIndex : obstacleNode.getVariationsToShow()){
                            for(ObjectMap.Entry<String, ObjectMap<Integer, GemNode>> entry1 : locationNode.getGems()){
                                if (entry1.value.containsKey(variationIndex)){
                                    entry1.value.get(variationIndex).setDefaultInvisible();
                                    //TODO 06 check
                                }
                            }
                        }
                    }

                    //keys
                    if (locationNode.getKeys().containsKey(name)){
                        entry.value.setKey(true);
                    }
                }
            }
        }

    }

    private void parseMeta(LocationNode location, XmlReader.Element locationElement){
        Gdx.app.log("LocationEditor", "meta.xml...");
        //first_location
        String firstRoom = locationElement.getAttribute("first_room", "");
        if (!firstRoom.equals("")){
            location.setFirstRoomName(firstRoom);
        }

        //iterate by variations special weight
        parseMeta_variationsSpecialWeight(location, locationElement.getChildByName("variation_special_weights"));

        //iterate by rooms
        XmlReader.Element roomsRootElement = locationElement.getChildByName("rooms");
        if (roomsRootElement != null){
            for(XmlReader.Element roomElement : roomsRootElement.getChildrenByName("room")){
                //find Room
                String roomName = roomElement.get("badge");
                RoomNode room = location.getRoom(roomName);
                Gdx.app.log("LocationEditor", "  room: " + roomName);
                //frames
                XmlReader.Element framesRootElement = roomElement.getChildByName("frames");
                if (framesRootElement != null){
                    for(XmlReader.Element frameElement : framesRootElement.getChildrenByName("frame")){
                        //find FrameNode
                        int frameIndex = Integer.parseInt(frameElement.get("index"));
                        Gdx.app.log("LocationEditor", "    frame: " + frameIndex);

                        //face palm again
                        FrameNode frameNode = null;
                        for(FrameNode contenderFrameNode : room.getFrames()){
                            if (contenderFrameNode.getIndex() == frameIndex){
                                frameNode = contenderFrameNode;
                            }
                        }

                        //obstacles
                        XmlReader.Element obstaclesRootElement = frameElement.getChildByName("obstacles");
                        if (obstaclesRootElement != null){
                            Gdx.app.log("LocationEditor", "      Obstacles root element exists");
                            for(XmlReader.Element obstacleElement : obstaclesRootElement.getChildrenByName("obstacle")){
                                String name = obstacleElement.getAttribute("name");
                                ObstacleNode obstacleNode = frameNode.getObstacle(name);
                                if (obstacleNode == null){
                                    Gdx.app.error("LocationEditor", "No obstacle named " + name);
                                }
                                Gdx.app.log("LocationEditor", "      Obstacle: " + name);

                                String crowbarLevelString = obstacleElement.getAttribute("crowbar_level", "");
                                if (!crowbarLevelString.equals("")){
                                    int crowbarLevel = Integer.parseInt(crowbarLevelString);
                                    obstacleNode.setCrowbar(true);
                                    obstacleNode.setCrowbarLevel(crowbarLevel);
                                }

                                String type = obstacleElement.getAttribute("type", "");
                                if (type.equals("3")){
                                    obstacleNode.setType(3);
                                }

                                XmlReader.Element variationsToShowRootElement = obstacleElement.getChildByName("variations_to_show");
                                if (variationsToShowRootElement != null){
                                    for(XmlReader.Element variationToShowElement : variationsToShowRootElement.getChildrenByName("variation_to_show")){
                                        int variationToShow = variationToShowElement.getIntAttribute("number");
                                        obstacleNode.addVariationToShow(variationToShow);
                                    }
                                }

                                XmlReader.Element gemsToShowRootElement = obstacleElement.getChildByName("gems_to_show");
                                if (gemsToShowRootElement != null){
                                    for(XmlReader.Element gemToShowElement : gemsToShowRootElement.getChildrenByName("gem_to_show")){
                                        String gemName = gemToShowElement.get("name");
                                        int variation =  gemToShowElement.getInt("variation");
                                        Gdx.app.log("LocationEditor", "gem to show " + gemName + " " + variation);
                                        obstacleNode.addGemToShow(gemName, variation);
                                    }
                                }
                            }
                        }
                    }
                }


            }
        }
        Gdx.app.log("LocationEditor", "...meta.xml");
    }

    private void parseMeta_variationsSpecialWeight(LocationNode location, XmlReader.Element root){
        if (root != null){
            for(XmlReader.Element element : root.getChildrenByName("variation_special_weight")){
                int index = element.getIntAttribute("index");
                int value = element.getIntAttribute("value", 5);
                location.addVariationSpecialWeight(index, value);
            }
        }
    }

    public VisWindow getComponentsWindow() {
        return componentsWindow;
    }

    public void setComponentsWindowContent(Actor content, String title){
        componentsWindow.clearChildren();
        componentsWindow.getTitleLabel().setText(title);
        componentsWindow.add(content).expand().align(Align.topLeft);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update(true);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        batch.dispose();
        stage.dispose();
    }

    void save(){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Iterator<LocationNode> locationNodeIterator = locations.values().iterator();
            if (locationNodeIterator.hasNext()){
                LocationNode locationNode = locationNodeIterator.next();

                String rootDirectoryPath = "locations/" + locationNode.getLocationName();
                String locationFilePath = rootDirectoryPath + "/location.xml";

                deleteDirectoryIfExist(rootDirectoryPath);
                createDirectoryIfNeed(rootDirectoryPath);
                saveFile = createFileIfNeed(locationFilePath);

                Element locationElement = document.createElement("location");
                locationElement.setAttribute("badge", locationNode.getLocationName());
                locationElement.setAttribute("first_room", locationNode.getFirstRoomName());
                document.appendChild(locationElement);

                //variation special weight
                Element specialWeightsRootElement = document.createElement("variation_special_weights");
                locationElement.appendChild(specialWeightsRootElement);
                for(ObjectMap.Entry<Integer, Integer> entry : locationNode.getVariationSpecialWeights()){
                    Element element = document.createElement("variation_special_weight");
                    element.setAttribute("index", entry.key + "");
                    element.setAttribute("value", entry.value + "");
                    specialWeightsRootElement.appendChild(element);
                }


                Element roomsCoreElement = document.createElement("rooms");
                locationElement.appendChild(roomsCoreElement);

                for(RoomNode roomNode : locationNode.getRooms()){
                    Element roomElement = document.createElement("room");
                    roomElement.setAttribute("badge", roomNode.getRoomName());
                    roomsCoreElement.appendChild(roomElement);
                    saveRoom(document, roomNode, roomElement);
                }

                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                DOMSource domSource = new DOMSource(document);

                StreamResult result = new StreamResult(saveFile.file());
                transformer.transform(domSource, result);
                Gdx.app.log("SAVE", saveFile.file().getAbsolutePath());

                saveFiles(locationNode);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e){
            e.printStackTrace();
        } catch (TransformerException e){
            e.printStackTrace();
        }
    }

    void saveRoom(Document document, RoomNode roomNode, Element roomElement){
        Element framesRootElement = document.createElement("frames");
        roomElement.appendChild(framesRootElement);
        for(FrameNode frameNode : roomNode.getFrames()){
            Element frameElement = document.createElement("frame");
            frameElement.setAttribute("index", frameNode.getIndex() + "");
            framesRootElement.appendChild(frameElement);

            //passages
            Element passagesRootElement = document.createElement("passages");
            frameElement.appendChild(passagesRootElement);
            for(int j = 0; j < frameNode.getPassageNodes().size; j++){
                PassageNode node = frameNode.getPassageNodes().get(j);
                Element passageElement = document.createElement("passage");
                passageElement.setAttribute("target", node.getRoomName());
                passageElement.setAttribute("frame", node.getOpeningFrameIndex() + "");
                passageElement.setAttribute("x", node.posX + "");
                passageElement.setAttribute("y", node.posY + "");
                passageElement.setAttribute("w", node.w + "");
                passageElement.setAttribute("h", node.h + "");
                passagesRootElement.appendChild(passageElement);
            }

            //goods
            Element gameObjectsElement = document.createElement("goods");
            frameElement.appendChild(gameObjectsElement);
            for(int j = 0; j < frameNode.getGameObjectNodes().size; j++){
                GameObjectNode gameObjectNode = frameNode.getGameObjectNodes().get(j);
                Element goElement = document.createElement("good");
                Gdx.app.log("Save", "      good " + gameObjectNode.getName());
                goElement.setAttribute("name", gameObjectNode.getName());
                goElement.setAttribute("x", gameObjectNode.posX + "");
                goElement.setAttribute("y", gameObjectNode.posY + "");
                goElement.setAttribute("variation", gameObjectNode.variation);
                gameObjectsElement.appendChild(goElement);
            }

            //gems
            Element gemsRootElement = document.createElement("gems");
            frameElement.appendChild(gemsRootElement);
            for(GemNode gemNode : frameNode.getGemNodes()){
                Element gemElement = document.createElement("gem");
                Gdx.app.log("Save", "      gem " + gemNode.getName());
                gemElement.setAttribute("name", gemNode.getName());
                gemElement.setAttribute("x", gemNode.getX() + "");
                gemElement.setAttribute("y", gemNode.getY() + "");
                gemElement.setAttribute("variation", gemNode.getVariation() + "");
                if (!gemNode.isDefaultVisible()){
                    gemElement.setAttribute("default_visible", "false");
                }
                gemsRootElement.appendChild(gemElement);
            }

            //pack
            Element packsRootElement = document.createElement("packs");
            frameElement.appendChild(packsRootElement);
            for(PackNode packNode : frameNode.getPackNodes()){
                Element packElement = document.createElement("pack");
                packElement.setAttribute("x", packNode.getX() + "");
                packElement.setAttribute("y", packNode.getY() + "");
                packElement.setAttribute("variation", packNode.getVariation() + "");
                if (!packNode.isDefaultVisible()){
                    packElement.setAttribute("default_visible", "false");
                }
                packsRootElement.appendChild(packElement);
            }

            //coins
            Element coinsRootElement = document.createElement("coins");
            frameElement.appendChild(coinsRootElement);
            for(CoinNode coinNode : frameNode.getCoinNodes()){
                Element coinElement = document.createElement("coin");
                coinElement.setAttribute("x", coinNode.getX() + "");
                coinElement.setAttribute("y", coinNode.getY() + "");
                coinElement.setAttribute("variation", coinNode.getVariation() + "");
                coinsRootElement.appendChild(coinElement);
            }

            //keys
            Element keysRootElement = document.createElement("keys");
            frameElement.appendChild(keysRootElement);
            for(KeyNode node : frameNode.getKeyNodes()){
                Element element = document.createElement("key");
                element.setAttribute("name", node.getName());
                element.setAttribute("x", node.x + "");
                element.setAttribute("y", node.y + "");
                element.setAttribute("variation", node.getVariation());
                keysRootElement.appendChild(element);
            }

            //zoom_triggers
            Element zoomTriggers = document.createElement("zoom_triggers");
            frameElement.appendChild(zoomTriggers);
            for(int j = 0; j < frameNode.getClickZoneNodes().size; j++){
                ZoomTriggerNode node = frameNode.getClickZoneNodes().get(j);
                Element czElement = document.createElement("zoom_trigger");
                czElement.setAttribute("name", node.getZtName());
                czElement.setAttribute("x", node.posX + "");
                czElement.setAttribute("y", node.posY + "");
                czElement.setAttribute("w", node.w + "");
                czElement.setAttribute("h", node.h + "");
                zoomTriggers.appendChild(czElement);
            }

            //obstacles
            Element obstaclesElement = document.createElement("obstacles");
            frameElement.appendChild(obstaclesElement);
            for(ObstacleNode node : frameNode.getObstacleNodesByOrder()){
                Element obstacleElement = document.createElement("obstacle");
                obstacleElement.setAttribute("name", node.getName());
                obstacleElement.setAttribute("x", node.posX + "");
                obstacleElement.setAttribute("y", node.posY + "");

                if (node.isHasOpenedTexture()) {
                    obstacleElement.setAttribute("has_opened_texture", "true");
                    obstacleElement.setAttribute("x1", node.openX + "");
                    obstacleElement.setAttribute("y1", node.openY + "");
                }
                obstacleElement.setAttribute("blocks", node.getBlocks());
                obstacleElement.setAttribute("blocked", node.isBlocked() + "");
                obstacleElement.setAttribute("type", node.getType() + "");
                obstacleElement.setAttribute("crowbar_level", node.getCrowbarLevel() + "");

                if (node.hasGemsToShow()){
                    Element gemsToShowRootElement = document.createElement("gems_to_show");

                    for(VariableObjectPointer variableObjectPointer : node.getGemsToShow()){
                        Element gemToShowElement = document.createElement("gem_to_show");
                        gemToShowElement.setAttribute("name", variableObjectPointer.getName());
                        gemToShowElement.setAttribute("variation", variableObjectPointer.getVariation() + "");
                        gemsToShowRootElement.appendChild(gemToShowElement);
                    }
                    obstacleElement.appendChild(gemsToShowRootElement);
                }

                if (node.hasVariationsToShow()){
                    Element variationsToShowRootElement = document.createElement("variations_to_show");

                    for(Integer variation : node.getVariationsToShow()){
                        Element variationElement = document.createElement("variation_to_show");
                        variationElement.setAttribute("number", variation + "");
                        variationsToShowRootElement.appendChild(variationElement);
                    }
                    obstacleElement.appendChild(variationsToShowRootElement);
                }

                obstaclesElement.appendChild(obstacleElement);
            }

            //covers
            Element coversRootElement = document.createElement("covers");
            frameElement.appendChild(coversRootElement);
            for(CoverNode coverNode : frameNode.getCoverNodes()){
                Element coverElement = document.createElement("cover");
                coverElement.setAttribute("name", coverNode.getName());
                coverElement.setAttribute("x", coverNode.getX() + "");
                coverElement.setAttribute("y", coverNode.getY() + "");
                coversRootElement.appendChild(coverElement);
            }

            //letter
            Element lettersRootElement = document.createElement("letters");
            frameElement.appendChild(lettersRootElement);
            for(LetterNode letterNode : frameNode.getLetterNodes()){
                Element letterElement = document.createElement("letter");
                letterElement.setAttribute("name", letterNode.getName());
                letterElement.setAttribute("variation", letterNode.getVariation() + "");
                letterElement.setAttribute("x", letterNode.getX() + "");
                letterElement.setAttribute("y", letterNode.getY() + "");
                letterElement.setAttribute("x1", letterNode.getX1() + "");
                letterElement.setAttribute("y1", letterNode.getY1() + "");
                lettersRootElement.appendChild(letterElement);
            }
        }

        Element zoomsRootElement = document.createElement("zooms");
        roomElement.appendChild(zoomsRootElement);
        for(ObjectMap.Entry<String, ZoomNode> entry : roomNode.getZooms()){
            Element zoomElement = document.createElement("zoom");
            ZoomNode zoomNode = entry.value;
            Gdx.app.log("LocationEditor", "ZoomNode: " + zoomNode.getName());
            zoomElement.setAttribute("badge", zoomNode.getName());
            zoomsRootElement.appendChild(zoomElement);

            //gems
            Element gemsRootElement = document.createElement("gems");
            zoomElement.appendChild(gemsRootElement);
            for(GemNode gemNode : zoomNode.getGems()){
                Gdx.app.log("LocationEditor", "gem: " + gemNode.getName());
                Element gemElement = document.createElement("gem");
                gemElement.setAttribute("name", gemNode.getName());
                gemElement.setAttribute("x", gemNode.getX() + "");
                gemElement.setAttribute("y", gemNode.getY() + "");
                gemElement.setAttribute("variation", gemNode.getVariation() + "");
                gemsRootElement.appendChild(gemElement);
            }

            //packs
            Element packsRootElement = document.createElement("packs");
            zoomElement.appendChild(packsRootElement);
            for(PackNode packNode : zoomNode.getPacks()){
                Element packElement = document.createElement("pack");
                packElement.setAttribute("x", packNode.getX() + "");
                packElement.setAttribute("y", packNode.getY() + "");
                packElement.setAttribute("variation", packNode.getVariation() + "");
                if (!packNode.isDefaultVisible()){
                    packElement.setAttribute("default_visible", "false");
                }
                packsRootElement.appendChild(packElement);
            }

            //coins
            Element coinsRootElement = document.createElement("coins");
            zoomElement.appendChild(coinsRootElement);
            for(CoinNode coinNode : zoomNode.getCoins()){
                Element coinElement = document.createElement("coin");
                coinElement.setAttribute("x", coinNode.getX() + "");
                coinElement.setAttribute("y", coinNode.getY() + "");
                coinElement.setAttribute("variation", coinNode.getVariation() + "");
                coinsRootElement.appendChild(coinElement);
            }

            //covers
            Element coversRootElement = document.createElement("covers");
            zoomElement.appendChild(coversRootElement);
            for(CoverNode coverNode : zoomNode.getCovers()){
                Element coverElement = document.createElement("cover");
                coverElement.setAttribute("name", coverNode.getName());
                coverElement.setAttribute("x", coverNode.getX() + "");
                coverElement.setAttribute("y", coverNode.getY() + "");
                coversRootElement.appendChild(coverElement);
            }
        }
    }

    private FileHandle createDirectoryIfNeed(String path){
        FileHandle result = Gdx.files.local(path);

        if (!result.exists()){
            result = new FileHandle(path);
            result.mkdirs();
        }

        return result;
    }

    private void deleteDirectoryIfExist(String path){
        FileHandle dir = Gdx.files.local(path);

        if (dir.exists()){
            dir.deleteDirectory();
        }
    }

    private FileHandle createFileIfNeed(String path){
        FileHandle result = Gdx.files.local(path);

        if (!result.exists()){
            result = new FileHandle(path);
        }

        return result;
    }

    private void saveFiles(LocationNode locationNode){
        String rootDirectoryPath = "locations/" + locationNode.getLocationName() + "/";

        //String keyIconsDirectoryPath = rootDirectoryPath + "key_icons/";
        //createDirectoryIfNeed(keyIconsDirectoryPath);
        String roomsRootDirectoryPath = rootDirectoryPath + "rooms/";
        createDirectoryIfNeed(roomsRootDirectoryPath);

        String sourceRoot = locationNode.getSourcePath();

        for(RoomNode roomNode : locationNode.getRooms()){
            String roomName = roomNode.getRoomName();
            String roomDirectoryPath = roomsRootDirectoryPath + roomName + "/";
            createDirectoryIfNeed(roomDirectoryPath);
            String zoomsRootDirectory = roomDirectoryPath + "zooms/";
            createDirectoryIfNeed(zoomsRootDirectory);

            String sourceRoom = sourceRoot + roomName + "/";

            //frames
            for(FrameNode frameNode : roomNode.getFrames()){
                String frameName = String.valueOf(frameNode.getIndex());
                String framePath = roomDirectoryPath + frameName + "/";

                String sourceFrame = sourceRoom + frameName + "/";

                //FileHandle source = Gdx.files.absolute(sourceFrame + "bg.jpg");
                //FileHandle target = Gdx.files.local(framePath + "bg.jpg");
                //source.copyTo(target);


                String objectsRootDirectory;

                //goods
                objectsRootDirectory = framePath + "goods/";
                createDirectoryIfNeed(objectsRootDirectory);
                for(GameObjectNode goodNode : frameNode.getGameObjectNodes()){
                    String base = goodNode.getName() + "_" + goodNode.variation;

                    copyPngFile(sourceFrame + "go_" + base, objectsRootDirectory + base);
                }

                //gems
                objectsRootDirectory = framePath + "gems/";
                createDirectoryIfNeed(objectsRootDirectory);
                for(GemNode gemNode : frameNode.getGemNodes()){
                    String base = gemNode.getName() + "_" + gemNode.getVariation();

                    copyPngFile(sourceFrame + "gem_" + base, objectsRootDirectory + base);
                }

                //pack
                objectsRootDirectory = framePath + "packs/";
                createDirectoryIfNeed(objectsRootDirectory);
                for(PackNode packNode : frameNode.getPackNodes()){
                    String base = packNode.getVariation() + "";

                    copyPngFile(sourceFrame + "pack_" + base, objectsRootDirectory + "pack_" + base);
                }

                //coin
                objectsRootDirectory = framePath + "coins/";
                createDirectoryIfNeed(objectsRootDirectory);
                for(CoinNode coinNode : frameNode.getCoinNodes()){
                    String var = coinNode.getVariation() + "";

                    copyPngFile(sourceFrame + "coin_" + var, objectsRootDirectory + "coin_" + var);
                }

                //keys
                objectsRootDirectory = framePath + "keys/";
                createDirectoryIfNeed(objectsRootDirectory);
                for(KeyNode keyNode : frameNode.getKeyNodes()){
                    String name = keyNode.getName();
                    String base = name + "_" + keyNode.getVariation();

                    copyPngFile(sourceFrame + "key_" + base, objectsRootDirectory + base);

                    //icon
                    base = "key_icons/" + name + ".png";
                    FileHandle keyIconHandle = Gdx.files.local(rootDirectoryPath + base);
                    if (!keyIconHandle.exists()){
                        FileHandle keyIconSourceHandle = Gdx.files.absolute(sourceRoot + base);
                        if (keyIconSourceHandle.exists()) {
                            keyIconSourceHandle.copyTo(keyIconHandle);
                        }else{
                            Gdx.app.error(TAG_VITYA, "No key icon for " + name);
                        }
                    }
                }

                //obstacles
                objectsRootDirectory = framePath + "obstacles/";
                createDirectoryIfNeed(objectsRootDirectory);
                //for(ObstacleNode obstacleNode : frameNode.getObstacleNodes()){
                for(ObjectMap.Entry<String, ObstacleNode> obstacleNodeEntry : frameNode.getObstacleNodes()){
                    String name = obstacleNodeEntry.key;

                    copyPngFile(sourceFrame + "ob_" + name + "_c", objectsRootDirectory + name + "_c");

                    if (obstacleNodeEntry.value.isHasOpenedTexture()){
                        copyPngFile(sourceFrame + "ob_" + name + "_o", objectsRootDirectory + name + "_o");
                    }
                }

                //covers
                objectsRootDirectory = framePath + "covers/";
                createDirectoryIfNeed(objectsRootDirectory);
                for(CoverNode coverNode : frameNode.getCoverNodes()){
                    String name = coverNode.getName();

                    copyPngFile(sourceFrame + name, objectsRootDirectory + name);
                }

                //letters
                objectsRootDirectory = framePath + "letters/";
                createDirectoryIfNeed(objectsRootDirectory);
                for(LetterNode letterNode : frameNode.getLetterNodes()){
                    String name = letterNode.getName();
                    int variation = letterNode.getVariation();
                    String base = name + "_" + variation;
                    copyPngFile(sourceFrame + "l_" + base + "_t", objectsRootDirectory + base + "_t");
                    copyPngFile(sourceFrame + "l_" + base + "_a", objectsRootDirectory + base + "_a");
                }


                File targetBG = Gdx.files.local(framePath + "bg.jpg").file();
                try {
                    targetBG.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                PngToJpg.convert(sourceFrame + "bg.png", targetBG);


                //zooms
                String zoomObjectsRootDirectory;
                for(ZoomTriggerNode zoomTriggerNode : frameNode.getClickZoneNodes()){
                    String name = zoomTriggerNode.getZtName();
                    String zoomDirectory = zoomsRootDirectory + name + "/";
                    createDirectoryIfNeed(zoomDirectory);

                    copyPngFile(sourceFrame + name + "/bg", zoomDirectory + "bg");

                    ZoomNode zoomNode = roomNode.getZoom(name);

                    //gems
                    zoomObjectsRootDirectory = zoomDirectory + "gems/";
                    createDirectoryIfNeed(zoomObjectsRootDirectory);
                    for(GemNode gemNode : zoomNode.getGems()){
                        String base = gemNode.getName() + "_" + gemNode.getVariation();

                        copyPngFile(sourceFrame + name + "/gem_" + base, zoomObjectsRootDirectory + base);
                    }

                    //packs
                    zoomObjectsRootDirectory = zoomDirectory + "packs/";
                    createDirectoryIfNeed(zoomObjectsRootDirectory);
                    for(PackNode packNode : zoomNode.getPacks()){
                        String base = "" + packNode.getVariation();

                        copyPngFile(sourceFrame + name + "/pack_" + base, zoomObjectsRootDirectory + "/pack_" + base);
                    }

                    //coins
                    zoomObjectsRootDirectory = zoomDirectory + "coins/";
                    createDirectoryIfNeed(zoomObjectsRootDirectory);
                    for(CoinNode coinNode : zoomNode.getCoins()){
                        String var = "" + coinNode.getVariation();

                        copyPngFile(sourceFrame + name + "/coin_" + var, zoomObjectsRootDirectory + "/coin_" + var);
                    }

                    //covers
                    zoomObjectsRootDirectory = zoomDirectory + "covers/";
                    createDirectoryIfNeed(zoomObjectsRootDirectory);
                    for(CoverNode coverNode : zoomNode.getCovers()){
                        String coverNodeName = coverNode.getName();
                        copyPngFile(sourceFrame + name + "/" + coverNodeName, zoomObjectsRootDirectory + coverNodeName);
                    }
                }
            }
        }

        //common pack files
        String packDirectoryPath = rootDirectoryPath + "pack/";
        createDirectoryIfNeed(packDirectoryPath);

        FileHandle sourcePack = Gdx.files.absolute(sourceRoot + "pack/");
        if (sourcePack.exists()){
            String source = sourcePack + "/opened";
            FileHandle file = Gdx.files.absolute(source + ".png");
            if (file.exists()) {
                copyPngFile(source, packDirectoryPath + "opened");
            }else{
                Gdx.app.error(TAG_VITYA, "Не правильно назвал файл opened.png или вообще не положил");
            }
            source = sourcePack + "/closed";
            file = Gdx.files.absolute(source + ".png");
            if (file.exists()) {
                copyPngFile(source, packDirectoryPath + "closed");
            }else{
                Gdx.app.error(TAG_VITYA, "Не правильно назвал файл closed.png или вообще не положил");
            }
        }else {
            Gdx.app.error(TAG_VITYA, "No pack folder");
        }
    }

    private void copyPngFile(String source, String target){
        FileHandle sourceHandle = Gdx.files.absolute(source + ".png");
        FileHandle targetHandle = Gdx.files.local(target + ".png");
        sourceHandle.copyTo(targetHandle);
    }

    private void copyJpgFile(String source, String target){
        FileHandle sourceHandle = Gdx.files.absolute(source + ".jpg");
        FileHandle targetHandle = Gdx.files.local(target + ".jpg");
        sourceHandle.copyTo(targetHandle);
    }

    public ZoomNode createZoom(String roomName, String zoomName) {
        ZoomNode zoomNode = new ZoomNode(zoomName);
        RoomNode roomNode = currentLocation.getRoom(roomName);
        roomNode.addZoom(zoomNode);

        return zoomNode;
    }
}
