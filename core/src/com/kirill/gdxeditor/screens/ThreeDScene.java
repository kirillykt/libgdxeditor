package com.kirill.gdxeditor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalShadowLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;


/**
 * Created by Kirill on 27.10.2015.
 */
public class ThreeDScene implements Screen {

    PerspectiveCamera camera;
    float w = 1280, h = 720f;

    Model model, ground;
    ModelInstance modelInstance, groundInstance[][];
    ModelBatch modelBatch;
    Environment environment;

    final int X_SIZE = 100, Z_SIZE = 100;

    @Override
    public void show() {
        camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.near = 0.1f;
        camera.far = 300f;

        camera.position.set(10, 10, 10);
        camera.lookAt(0, 0, 0);
        camera.update();
        ModelBuilder modelBuilder = new ModelBuilder();
        model = modelBuilder.createBox(5f, 5f, 5f, new Material(TextureAttribute.createDiffuse(new Texture("rock.png"))),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates);
        TextureAttribute attribute = TextureAttribute.createDiffuse(new Texture("grass.png"));

        ground = modelBuilder.createBox(1f, 1f, 1f, new Material(attribute), VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates);
        groundInstance = new ModelInstance[100][100];
        for(int x = 0; x < X_SIZE; x++){
            for(int z = 0; z < Z_SIZE; z++){
                groundInstance[x][z] = new ModelInstance(ground);
                ModelInstance t = groundInstance[x][z];
                t.transform.set(new Vector3(x - 50, 0, z - 50), new Quaternion());
            }
        }
        modelInstance = new ModelInstance(model);
        modelBatch = new ModelBatch();
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        CameraInputController controller = new CameraInputController(camera);
        Gdx.input.setInputProcessor(controller);

        Gdx.gl.glClearColor(0, 0, 0, 1);
    }

    float elapsedTime = 0;
    float rotationX = 5f;
    @Override
    public void render(float delta) {
        elapsedTime += delta;
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        modelInstance.transform.rotate(1, 0, 0, rotationX * delta);
        modelBatch.begin(camera);
        for(int x = 0; x < X_SIZE; x++){
            for(int z = 0; z < Z_SIZE; z++){
            modelBatch.render(groundInstance[x][z], environment);
            }
        }
        modelBatch.render(modelInstance, environment);
        modelBatch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        model.dispose();
        modelBatch.dispose();
    }

    @Override
    public void dispose() {

    }
}
