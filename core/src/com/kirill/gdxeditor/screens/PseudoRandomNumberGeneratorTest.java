package com.kirill.gdxeditor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kirill.gdxeditor.utils.PseudoRandom;
import com.kotcrab.vis.ui.widget.VisLabel;

/**
 * Created by Kirill on 05.11.2015.
 */
public class PseudoRandomNumberGeneratorTest implements Screen {

    int[] numbers;
    int[] results;
    int size = 100;
    int range = 15;
    Stage stage;
    VisLabel label;
    FitViewport viewport;
    @Override
    public void show() {
        numbers = new int[size];
        for(int i = 0; i < size; i++){
            numbers[i] = PseudoRandom.random(range);
        }

        results = new int[range];
        for(int i = 0 ; i < size; i++){
            results[numbers[i]]++;
        }

        for(int i = 0; i < range; i++){
            System.out.println("i = " + i + " result: " + results[i]);
        }
        viewport = new FitViewport(1280f, 720f);

        stage = new Stage(viewport);

        label = new VisLabel("", Color.WHITE);
        label.setPosition(600, 360);
        stage.addActor(label);

        Gdx.gl.glClearColor(0, 0, 0, 1);
    }

    void sort(){
        for (int i = 0; i < range; i++){
            for(int j = 1; j < range; j++){
                if(results[j] > results[i]){
                    int tmp = results[j];
                    results[j] = results[i];
                    results[i] = tmp;
                }
            }
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
            label.setText("" + PseudoRandom.random(100));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
