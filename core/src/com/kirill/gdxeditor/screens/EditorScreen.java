package com.kirill.gdxeditor.screens;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.*;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kirill.gdxeditor.Editor;
import com.kirill.gdxeditor.ashley.EntityManager;
import com.kirill.gdxeditor.ashley.components.TransformComponent;
import com.kirill.gdxeditor.gui.AssetsWindow;
import com.kirill.gdxeditor.gui.InspectorWindow;
import com.kirill.gdxeditor.gui.TopMenuPanel;
import com.kirill.gdxeditor.gui.TransformArrows;
import com.kirill.gdxeditor.input.SimpleInputListener;
import com.kirill.gdxeditor.utils.RenderUtils;
import com.kotcrab.vis.ui.VisUI;

public class EditorScreen implements Screen, InputProcessor, GestureListener {

    Stage stage;

    //Assets

    AssetsWindow assetsWindow;

    //Top Menu
    TopMenuPanel topMenuPanel;

    //Inspector
    public InspectorWindow inspectorWindow;

    SpriteBatch batch;
    ShapeRenderer shapeRenderer;
    OrthographicCamera uiCamera;
    OrthographicCamera camera;
    FitViewport viewport;

    public EntityManager entityManager;

    public TransformArrows transformArrows;

    Color stepSmallColor, stepBigColor;

    public Array<Sprite> sprites;
    Array<GestureListener> gestureListeners;
    Array<SimpleInputListener> simpleInputListeners;

    public static float maxCoord = 128000f;
    public static float stepSmall = 64f;
    public static float stepBig = 256f;

    Color clearColor = new Color(Color.BLACK);

    @Override
    public void show() {

        uiCamera = new OrthographicCamera();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Editor.W, Editor.H, uiCamera);
        stage = new Stage(viewport);
        batch = new SpriteBatch();

        shapeRenderer = new ShapeRenderer();


        System.gc();
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        stepSmallColor = new Color(0.2f, 0.2f, 0.2f, 1);
        stepBigColor = new Color(0.45f, 0.45f, 0.45f, 1);


        sprites = new Array<Sprite>();
        gestureListeners = new Array<GestureListener>();
        simpleInputListeners = new Array<SimpleInputListener>();

        transformArrows = new TransformArrows(this);

        entityManager = new EntityManager(batch, camera);

        createActors();
        setupActors();
        addActors();
        InputMultiplexer multiplexer = new InputMultiplexer(stage, new GestureDetector(this), this);

        Gdx.input.setInputProcessor(multiplexer);
    }

    public void registerSimpleInputListener(SimpleInputListener listener) {
        simpleInputListeners.add(listener);
    }

    public static float assetsPaneWidth = 400f;
    public static float assetsPaneHeight = 400f;


    private void createActors() {
        //Menu
        topMenuPanel = new TopMenuPanel();
        //Assets
        assetsWindow = new AssetsWindow(this);
        //Inspector
        inspectorWindow = new InspectorWindow("Inspector", entityManager);

    }

    private void addActors() {
        stage.addActor(topMenuPanel);
        stage.addActor(assetsWindow);
        stage.addActor(inspectorWindow);
    }

    public static float menuBarHeight = 75f;
    public static float padding = 5f;

    private void setupActors() {
        VisUI.setDefaultTitleAlign(Align.left);
        //Assets


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.viewportWidth = 1920f;
        camera.viewportHeight = 1080f;
        camera.update();
        stage.act(delta);

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeType.Line);
        drawGrid();
        drawSelectedEntity();
        shapeRenderer.end();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        entityManager.update(delta);
        Entity selected = entityManager.getSelectedEntity();
        if (selected != null) {
            transformArrows.setActive(true);
            transformArrows.draw(batch, selected, camera.zoom);
        } else {
            transformArrows.setActive(false);
        }
        batch.end();

        stage.draw();
    }

    float shapeScale = 1.0f;

    void drawSelectedEntity() {
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.line(-maxCoord, 0, maxCoord, 0);
        shapeRenderer.line(0, -maxCoord, 0, maxCoord);
        shapeRenderer.setColor(Color.GREEN);
        if (entityManager.getSelectedEntity() != null) {
            Entity selectedEntity = entityManager.getSelectedEntity();
            TransformComponent transform = selectedEntity.getComponent(TransformComponent.class);

            float[] vertices =
                    RenderUtils.calculatePolygon(transform.position, transform.origin, transform.width + 2, transform.height + 2,
                            transform.scaleX * shapeScale, transform.scaleY * shapeScale, transform.rotation);
            shapeRenderer.polygon(vertices);
        }
    }

    void drawGrid() {
        shapeRenderer.setColor(stepSmallColor);
        for (float x = -maxCoord; x <= maxCoord; x += stepSmall) {
            shapeRenderer.line(x, -maxCoord, x, maxCoord);
        }

        for (float y = -maxCoord; y <= maxCoord; y += stepSmall) {
            shapeRenderer.line(-maxCoord, y, maxCoord, y);
        }

        shapeRenderer.setColor(stepBigColor);

        for (float x = -maxCoord; x <= maxCoord; x += stepBig) {
            shapeRenderer.line(x, -maxCoord, x, maxCoord);
        }

        for (float y = -maxCoord; y <= maxCoord; y += stepBig) {
            shapeRenderer.line(-maxCoord, y, maxCoord, y);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        if (batch != null)
            batch.dispose();
        if (stage != null)
            stage.dispose();
        viewport = null;
        camera = null;
        uiCamera = null;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }


    Vector3 touchPos = new Vector3();
    Vector3 deltaPos = new Vector3();
    Vector3 unprojected = new Vector3();

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touchPos.x = screenX;
        touchPos.y = screenY;

        for (int i = 0; i < gestureListeners.size; i++) {
            if (gestureListeners.get(i).touchDown(touchPos.x, touchPos.y, pointer, button)) {
                return true;
            }
        }

        unprojected.x = screenX;
        unprojected.y = screenY;
        unprojected = camera.unproject(unprojected);
        for (int i = 0; i < simpleInputListeners.size; i++) {
            if (simpleInputListeners.get(i).touchDown(unprojected.x, unprojected.y)) return true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        unprojected.x = screenX;
        unprojected.y = screenY;
        unprojected = camera.unproject(unprojected);

        for (int i = 0; i < simpleInputListeners.size; i++) {
            simpleInputListeners.get(i).touchUp(unprojected.x, unprojected.y);
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        deltaPos.x = screenX;
        deltaPos.y = screenY;


        deltaPos.x = touchPos.x - deltaPos.x;
        deltaPos.y = deltaPos.y - touchPos.y;

        if (Gdx.input.isButtonPressed(Input.Buttons.MIDDLE) && Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) {
            deltaPos.scl(3f * camera.zoom);
            camera.translate(deltaPos.x, deltaPos.y);
        }

        if (Gdx.input.isButtonPressed(Input.Buttons.MIDDLE)) {
            camera.translate(deltaPos.x, deltaPos.y);
        }


        touchPos.x = screenX;
        touchPos.y = screenY;

        unprojected.x = screenX;
        unprojected.y = screenY;
        unprojected = camera.unproject(unprojected);

        for (int i = 0; i < simpleInputListeners.size; i++) {
            simpleInputListeners.get(i).touchDragged(unprojected.x, unprojected.y, deltaPos.x, deltaPos.y);
        }

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if (stage.scrolled(amount)) return false;

        if (camera.zoom + (float) amount / 10f > 0)
            camera.zoom += (float) amount / 10f;
        return false;
    }


    Vector2 touchDownVector2 = new Vector2();

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        touchDownVector2.x = x;
        touchDownVector2.y = y;
        Rectangle assetsWindowBounds = new Rectangle(0, 0, assetsWindow.getWidth(), assetsWindow.getHeight());
        if (!assetsWindowBounds.contains(assetsWindow.screenToLocalCoordinates(touchDownVector2)))
            stage.setScrollFocus(null);
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        touchPos.x = x;
        touchPos.y = y;
        Vector3 unproject = camera.unproject(new Vector3(x, y, 0));
        for (int i = 0; i < gestureListeners.size; i++) {
            if (gestureListeners.get(i).tap(unproject.x, unproject.y, count, button)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
                         Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

}
