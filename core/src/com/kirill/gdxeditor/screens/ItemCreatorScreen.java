package com.kirill.gdxeditor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlWriter;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kirill.gdxeditor.entities.ItemRow;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by Kirill on 06.10.2015.
 */
public class ItemCreatorScreen implements Screen {
    FitViewport viewport;
    OrthographicCamera camera;

    Stage stage;
    VisTable table, titleTable, buttonsTable;
    VisScrollPane scrollPane;
    VisLabel numLabel, iconLabel, idLabel, nameLabel, newNameLabel, priceLabel, removeLabel, specialLabel;
    VisTextButton addBtn, saveBtn;
    public Texture browseTexture;
    public FileChooser fileChooser;

    FileHandle saveFile;
    String saveFilePath = "good_bank/bank.xml";
    public Array<ItemRow> itemRows;

    public static final float W = 1280f, H = 720f, TABLE_H = 38f, TABLE_W = 1100f;
    public static final float numW = 32f, iconW = 38f, nameW = 250f, idW = 250f, priceW = 128f, removeW = 32f, specialItemCBW = 96f;
    boolean inited = false;

    @Override
    public void show() {
        inited = true;
        camera = new OrthographicCamera(W, H);
        viewport = new FitViewport(W, H, camera);
        stage = new Stage(viewport);
        table = new VisTable(true);
        browseTexture = new Texture("browse.png");
        Gdx.input.setInputProcessor(stage);
        fileChooser = new FileChooser(FileChooser.Mode.OPEN);
        fileChooser.setSelectionMode(FileChooser.SelectionMode.FILES);
        fileChooser.setMultiselectionEnabled(true);
        itemRows = new Array<ItemRow>();
        setupActors();

        saveFile = Gdx.files.local(saveFilePath);
        load();
    }

    void setupActors(){
        scrollPane = new VisScrollPane(table);
        stage.addActor(scrollPane);
        scrollPane.setSize(W, 600);
        table.setSize(W, 600);
        scrollPane.setPosition(0, H - scrollPane.getHeight());
        scrollPane.setFadeScrollBars(false);
        stage.setScrollFocus(scrollPane);
        table.align(Align.topLeft);
        table.defaults().expandX();


        titleTable = new VisTable(true);
        numLabel = new VisLabel("#");
        iconLabel = new VisLabel("Icon");
        idLabel = new VisLabel("Name");
        nameLabel = new VisLabel("Caption");
        newNameLabel = new VisLabel("New Caption");
        priceLabel = new VisLabel("Price");
        specialLabel = new VisLabel("Was Used");
        removeLabel = new VisLabel("X");
        titleTable.setSize(W, TABLE_H);
        titleTable.align(Align.topLeft);
        titleTable.defaults().height(TABLE_H).expandX();
        titleTable.add(numLabel).width(numW);

        titleTable.add(iconLabel).width(iconW);
        titleTable.add(idLabel).width(idW);
        titleTable.add(nameLabel).width(nameW);
        titleTable.add(newNameLabel).width(nameW);
        titleTable.add(priceLabel).width(priceW);
        titleTable.add(specialLabel).width(specialItemCBW);
        titleTable.add(removeLabel).width(removeW).row();

        table.add(titleTable).width(TABLE_W).row();

        addBtn = new VisTextButton("Add");
        addBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.addActor(fileChooser.fadeIn());
                fileChooser.setListener(new FileChooserAdapter(){
                    @Override
                    public void selected(Array<FileHandle> files) {
                        for(int i = 0; i < files.size; i++){
                            addNewItemRow(files.get(i));
                        }
                    }
                });
            }
        });
        saveBtn = new VisTextButton("Save");
        saveBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                save();
            }
        });
        VisTextButton sortButton = new VisTextButton("Sort");
        sortButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                sortByPrice();
            }
        });
        final VisTextField searchTextField = new VisTextField("Find...");
        searchTextField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                for (int i = 0; i < itemRows.size; i++) {
                    if (itemRows.get(i).getId().toLowerCase().contains(searchTextField.getText().toLowerCase())){
                        float offsetY = 0;
                        if(i > 11 && i < itemRows.size - 11)
                            offsetY = TABLE_H * 5;
                        scrollPane.setScrollY(i * (TABLE_H + 6) + offsetY + TABLE_H);
                        break;
                    }
                }
            }
        });
        buttonsTable = new VisTable(true);
        buttonsTable.align(Align.topLeft);
        buttonsTable.setPosition(table.getX(), scrollPane.getY() - 10);
        buttonsTable.add(addBtn);
        buttonsTable.add(saveBtn);
        buttonsTable.add(sortButton);
        buttonsTable.add(searchTextField).width(200);
        stage.addActor(buttonsTable);

    }

    void sortByPrice(){
        itemRows.sort();
        table.clearChildren();
        for(int i = 0; i < itemRows.size; i++){
            table.add(itemRows.get(i)).width(TABLE_W).row();
        }
        table.pack();
        save();
    }

    float elapsedSaveTime = 0;
    final float saveTimePeriod = 3f;
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        camera.update(true);
        elapsedSaveTime += delta;
        if(elapsedSaveTime > saveTimePeriod){
            save();
            elapsedSaveTime = 0;
        }
    }

    void addNewItemRow(){
        ItemRow row = new ItemRow(this);
        table.add(row).width(TABLE_W).row();
        itemRows.add(row);
        save();
    }

    void addNewItemRow(FileHandle file){
        ItemRow row = new ItemRow(this, file);
        table.add(row).width(TABLE_W).height(TABLE_H).row();
        itemRows.add(row);
        save();
    }

    public void removeExistingRow(ItemRow row){
        table.removeActor(row);
        itemRows.removeValue(row, true);
    }



    public void save(){
        if(!inited) return;
        for(int i = 0 ; i < itemRows.size; i++){
            itemRows.get(i).save();
        }
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("bank");
            document.appendChild(root);
            for(int i = 0; i < itemRows.size; i++){
                ItemRow row = itemRows.get(i);
                Element item = document.createElement("item");
                item.setAttribute("name", row.getId());
                item.setAttribute("priceCategory", row.getPrice() + "");
                item.setAttribute("wasUsed", row.wasUsed + "");
                item.setAttribute("num", row.getNum() + "");
                Element itemsCaptionsElement = document.createElement("captions");
                Element caption = document.createElement("caption");
                caption.setTextContent(row.getItemname());
                caption.setAttribute("lang", "eng");
                for (int j = 0; j < row.otherCaptions.size; j++) {
                    Element otherCaption = document.createElement("caption");
                    otherCaption.setAttribute("lang", row.otherCaptions.get(j).lang);
                    otherCaption.setTextContent(row.otherCaptions.get(j).name);
                    itemsCaptionsElement.appendChild(otherCaption);
                }
                for (int j = 0; j < row.newCaptions.size; j++) {
                    Element newCaption = document.createElement("new_caption");
                    newCaption.setAttribute("lang", row.newCaptions.get(j).lang);
                    newCaption.setTextContent(row.newCaptions.get(j).name);
                    itemsCaptionsElement.appendChild(newCaption);
                }
                Element newCaption = document.createElement("new_caption");
                newCaption.setTextContent(row.getNewName());
                newCaption.setAttribute("lang", "eng");
                itemsCaptionsElement.appendChild(caption);
                itemsCaptionsElement.appendChild(newCaption);
                item.appendChild(itemsCaptionsElement);
                root.appendChild(item);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource domSource = new DOMSource(document);
            StreamResult result = new StreamResult(saveFile.file());
            transformer.transform(domSource, result);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (TransformerConfigurationException e){
            e.printStackTrace();
        }
        catch (TransformerException e){
            e.printStackTrace();
        }

    }

    void load(){
        if(!saveFile.exists()){
            System.out.println("Save file not exists " + saveFile.file().getAbsolutePath());
            try {
                if(!saveFile.parent().exists())
                    saveFile.parent().file().mkdirs();
                saveFile.file().createNewFile();
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Loading " + saveFile.file().getAbsolutePath());
        XmlReader reader = new XmlReader();
        try {
            XmlReader.Element root = reader.parse(saveFile);
            if(root == null) {
                System.out.println("Root is null");
                return;
            }
            for(int i = 0; i < root.getChildCount(); i++){
                ItemRow row = new ItemRow(this);
                XmlReader.Element child = root.getChild(i);
                row.setPrice(child.getInt("priceCategory"));
                if(child.getChildByName("captions") != null) {
                    if (child.getChildByName("captions").getChildByName("caption") != null) {
                        XmlReader.Element captionsElement = child.getChildByName("captions");
                        Array<XmlReader.Element> captionsArray = captionsElement.getChildrenByNameRecursively("caption");
                        for (int j = 0; j < captionsArray.size; j++) {
                            if(captionsArray.get(j).get("lang").equals("eng")){
                                Gdx.app.log("Item name", captionsArray.get(j).getText());
                                row.setItemname(captionsArray.get(j).getText());
                            } else{
                                row.otherCaptions.add(new ItemRow.Caption(captionsArray.get(j).get("lang"), captionsArray.get(j).getText()));
                            }
                        }
                    }
                    if (child.getChildByName("captions").getChildByName("new_caption") != null) {
                        XmlReader.Element captionsElement = child.getChildByName("captions");
                        Array<XmlReader.Element> newCaptionsArray = captionsElement.getChildrenByNameRecursively("new_caption");
                        for (int j = 0; j < newCaptionsArray.size; j++) {
                            if(newCaptionsArray.get(j).get("lang").equals("eng")){
                                row.setNewName(newCaptionsArray.get(j).getText());
                                Gdx.app.log("Item new name", newCaptionsArray.get(j).getText());
                            } else{
                                row.newCaptions.add(new ItemRow.Caption(newCaptionsArray.get(j).get("lang"), newCaptionsArray.get(j).getText()));
                            }
                        }
                    }
                }
                row.setId(child.get("name"));
                row.setNum(i);
                row.wasUsed = child.getBoolean("wasUsed", false);
                row.reload();
                table.add(row).width(TABLE_W).row();
                itemRows.add(row);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        VisUI.dispose();
        stage.dispose();
    }

    @Override
    public void dispose() {

    }
}
