package com.kirill.gdxeditor.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.kirill.gdxeditor.entities.ItemRow;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;

/**
 * Created by Kirill on 03.11.2015.
 */
public class XmlReparser {

    Array<ItemRow> rows;

    public XmlReparser(){
        XmlReader reader = new XmlReader();
        rows = new Array<ItemRow>();
        try {
            XmlReader.Element root = reader.parse(Gdx.files.internal("items.xml"));
            for(int i = 0; i < root.getChildCount(); i++){
                ItemRow row = new ItemRow();
                rows.add(row);
                row.setupWithXml(root.getChild(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < rows.size; i++){
            int price = rows.get(i).getPrice();
            if(price >= 21 && price < 41){
                price += 80;
            } else if(price >= 41 && price < 61){
                price += 160;
            }
            rows.get(i).setPrice(price);
        }

        save();
    }

    public void save(){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("root");
            document.appendChild(root);
            for(int i = 0; i < rows.size; i++){
                ItemRow row = rows.get(i);
                Element item = document.createElement("Item");
                item.setAttribute("name", row.getId());
                item.setAttribute("price", row.getPrice() + "");
                //item.setAttribute("iconPath", row.iconPath);
                //item.setAttribute("iconName", row.iconFileName);
                item.setAttribute("num", row.getNum() + "");
                Element itemsCaptionsElement = document.createElement("Captions");
                Element caption = document.createElement("caption");
                caption.setTextContent(row.getItemname());
                caption.setAttribute("lang", "eng");
                itemsCaptionsElement.appendChild(caption);
                item.appendChild(itemsCaptionsElement);
                root.appendChild(item);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource domSource = new DOMSource(document);
            FileHandle saveFile = Gdx.files.local("items_new.xml");
            Gdx.app.log("SAVING", saveFile.file().getAbsolutePath());
            StreamResult result = new StreamResult(saveFile.file());
            transformer.transform(domSource, result);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (TransformerConfigurationException e){
            e.printStackTrace();
        }
        catch (TransformerException e){
            e.printStackTrace();
        }

    }

}
