package com.kirill.gdxeditor.utils;

/**
 * Created by Kirill on 05.11.2015.
 */
public class PseudoRandom {

    public static int random(int range){
        long currentTime = System.nanoTime();
        int returnValue = (int)(currentTime % range);
        return returnValue;
    }

    public static int random(int start, int end){
        int rnd = random(end);
        if(rnd >= start) return rnd;
        else return random(end);
    }
}
