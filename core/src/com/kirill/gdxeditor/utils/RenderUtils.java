package com.kirill.gdxeditor.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public class RenderUtils {
	
	public static final float[] vertices = new float[8];
	
	public static float[] calculatePolygon(Vector2 position, Vector2 origin, float width, float height, float scaleX, float scaleY, float rotation){
		Polygon polygon = new Polygon();
		
		
		float worldOriginX = position.x + origin.x - 1;
		float worldOriginY = position.y + origin.y - 1;
		float fx = -origin.x;
		float fy = -origin.y;
		float fx2 = width + fx;
		float fy2 = height + fy;

		// scale
		if (scaleX != 1 || scaleY != 1) {
			fx *= scaleX;
			fy *= scaleY;
			fx2 *= scaleX;
			fy2 *= scaleY;
		}

		// construct corner points, start from top left and go counter clockwise
		final float p1x = fx;
		final float p1y = fy;
		final float p2x = fx;
		final float p2y = fy2;
		final float p3x = fx2;
		final float p3y = fy2;
		final float p4x = fx2;
		final float p4y = fy;

		float x1;
		float y1;
		float x2;
		float y2;
		float x3;
		float y3;
		float x4;
		float y4;

		// rotate
		if (rotation != 0) {
			final float cos = MathUtils.cosDeg(rotation);
			final float sin = MathUtils.sinDeg(rotation);

			x1 = cos * p1x - sin * p1y;
			y1 = sin * p1x + cos * p1y;

			x2 = cos * p2x - sin * p2y;
			y2 = sin * p2x + cos * p2y;

			x3 = cos * p3x - sin * p3y;
			y3 = sin * p3x + cos * p3y;

			x4 = x1 + (x3 - x2);
			y4 = y3 - (y2 - y1);
		} else {
			x1 = p1x;
			y1 = p1y;

			x2 = p2x;
			y2 = p2y;

			x3 = p3x;
			y3 = p3y;

			x4 = p4x;
			y4 = p4y;
		}

		x1 += worldOriginX;
		y1 += worldOriginY;
		x2 += worldOriginX;
		y2 += worldOriginY;
		x3 += worldOriginX;
		y3 += worldOriginY;
		x4 += worldOriginX;
		y4 += worldOriginY;
		vertices[0] = x1;
		vertices[1] = y1;
		vertices[2] = x2;
		vertices[3] = y2;
		vertices[4] = x3;
		vertices[5] = y3;
		vertices[6] = x4;
		vertices[7] = y4;
		
		polygon.setVertices(vertices);
		
		return vertices;
	}

}
