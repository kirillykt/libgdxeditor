package com.kirill.gdxeditor.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;


import java.io.Writer;
import java.util.Iterator;

/**
 * Created by Kirill on 23.10.2015.
 */
public class LotrWordAnalysis {

    FileHandle lotrBook;
    FileHandle output;
    String wholeBook;
    Array<String> words;
    Array<Integer> timesUsed;

    ObjectMap<String, Integer> counts;
    String charset = "ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮёйцукенгшщзхъфывапролджэячсмитьбю";

    public LotrWordAnalysis(){
        lotrBook = Gdx.files.internal("anna_kar.txt");
        wholeBook = lotrBook.readString();

        String[] separateWords = wholeBook.split(" ");
        counts = new ObjectMap<String, Integer>();
        for(int i = 0; i < separateWords.length; i++){
            String word = separateWords[i];
            separateWords[i] = word.replaceAll("[-+.^:,]()!?","");
            separateWords[i] = word.replaceAll("!", "");
            separateWords[i] = word.replaceAll("\\?", "");
            separateWords[i] = word.replaceAll("\n", "");
            separateWords[i] = word.replaceAll(",", "");
            separateWords[i] = word.replaceAll(".", "");
            separateWords[i] = word.toLowerCase();
            double percent = (double)i / (double)separateWords.length * 100;
            Gdx.app.log("replacing and lowering", "" + percent);
        }

        for(int i = 0; i < separateWords.length; i++){
            if(separateWords[i].equals("")) continue;
            if(separateWords[i].equals("-")) continue;
            if(!counts.containsKey(separateWords[i])){
                counts.put(separateWords[i], 1);
            } else{
                int count = counts.get(separateWords[i]);
                count++;
                counts.put(separateWords[i], count);
            }
            double percent = (double)i / (double)separateWords.length * 100;
            Gdx.app.log("putting to objectmap", "" + percent);
        }
        output = Gdx.files.local("output_anna_kar.txt");

        Iterator<String> iterator = counts.keys().iterator();
        words = new Array<String>();
        timesUsed = new Array<Integer>();
        while (iterator.hasNext()){
            String word = iterator.next();
            words.add(word);
            timesUsed.add(counts.get(word));

        }

        sort();

        for(int i = 0; i < words.size; i++){
            output.writeString((i + 1) + ". " + words.get(i) + " = " + timesUsed.get(i) + "\n", true);
            double percent = (double)i / (double)words.size * 100;
            Gdx.app.log("writing", "" + percent);
        }
    }

    void sort(){
        for(int i = 0; i < timesUsed.size - 1; i++){
            for(int j = 0; j < timesUsed.size - i - 1; j++){
                if(timesUsed.get(j) < timesUsed.get(j + 1)){
                    timesUsed.swap(j, j + 1);
                    words.swap(j, j + 1);
                }
            }
        }
    }
}
