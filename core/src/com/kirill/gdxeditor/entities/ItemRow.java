package com.kirill.gdxeditor.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.kirill.gdxeditor.screens.ItemCreatorScreen;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;

import java.io.*;

/**
 * Created by Kirill on 06.10.2015.
 */
public class ItemRow extends VisTable implements Comparable<ItemRow>, Serializable{
    VisTextField numField, idField, nameField, newNameField;
    NumberSelector priceField;
    int num, price;
    String id, itemname;
    public String newName;
    public boolean wasUsed = false;
    VisImageButton browseButton;
    VisTextButton removeBtn;
    VisCheckBox wasUsedCheckBox;
    ItemCreatorScreen creatorScreen;
    public Array<Caption> otherCaptions;
    public Array<Caption> newCaptions;

    public ItemRow(){

    }

    public  ItemRow(final ItemCreatorScreen screen){
        super(true);
        otherCaptions = new Array<Caption>();
        newCaptions = new Array<Caption>();
        this.creatorScreen = screen;
        numField = new VisTextField(screen.itemRows.size + "");
        setNum(screen.itemRows.size);
        idField = new VisTextField("");
        nameField = new VisTextField("");
        newNameField = new VisTextField("");
        priceField = new NumberSelector("Cat:", 1, 1, 800, 1);
        browseButton = new VisImageButton(new TextureRegionDrawable(new TextureRegion(screen.browseTexture)));
        removeBtn = new VisTextButton("X");
        wasUsedCheckBox = new VisCheckBox("Was Used");
        wasUsedCheckBox.setChecked(false);
        wasUsedCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                wasUsed = wasUsedCheckBox.isChecked();
            }
        });

        setSize(ItemCreatorScreen.W, ItemCreatorScreen.TABLE_H);
        align(Align.topLeft);
        defaults().height(ItemCreatorScreen.TABLE_H).expandX();
        add(numField).width(ItemCreatorScreen.numW);
        add(browseButton).width(ItemCreatorScreen.iconW);
        add(idField).width(ItemCreatorScreen.idW);
        add(nameField).width(ItemCreatorScreen.nameW);
        add(newNameField).width(ItemCreatorScreen.nameW);
        add(priceField).width(ItemCreatorScreen.priceW);
        add(wasUsedCheckBox).width(ItemCreatorScreen.specialItemCBW);
        add(removeBtn).width(ItemCreatorScreen.removeW);

        browseButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ItemRow.this.getStage().addActor(ItemRow.this.creatorScreen.fileChooser.fadeIn());
                creatorScreen.fileChooser.setListener(new FileChooserAdapter(){
                    @Override
                    public void selected(FileHandle file) {
                        //iconPath = file.path();
                        //iconFileName = file.name();
                        setId(file.nameWithoutExtension());
                        copyFile(file, Gdx.files.local("good_bank/" + file.name()));
                        Texture texture = new Texture(file);
                        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(texture));
                        ItemRow.this.browseButton.setStyle(new VisImageButton.VisImageButtonStyle(drawable, drawable, drawable, drawable, drawable, drawable));
                    }
                });
            }
        });

        priceField.addChangeListener(new NumberSelector.NumberSelectorListener() {
            @Override
            public void changed(int number) {
                setPrice(priceField.getValue());
                screen.save();
            }
        });

        nameField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                setItemname(nameField.getText());
                screen.save();
            }
        });

        newNameField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                setNewName(newNameField.getText());
                screen.save();
            }
        });

        idField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                FileHandle fileHandle = Gdx.files.local("good_bank/" + getId() + ".png");
                fileHandle.copyTo(Gdx.files.local("good_bank/" + idField.getText() + ".png"));
                fileHandle.delete();
                setId(idField.getText());
                screen.save();
            }
        });
        removeBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                creatorScreen.removeExistingRow(ItemRow.this);
                screen.save();
            }
        });

    }

    /*public ItemRow(){

    }*/

    public ItemRow(ItemCreatorScreen screen, FileHandle file){
        this(screen);
        //iconPath = file.path();
        //iconFileName = file.name();
        copyFile(file, Gdx.files.external("itemscreator/" + file.name()));
        Texture texture = new Texture(file);
        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(texture));
        ItemRow.this.browseButton.setStyle(new VisImageButton.VisImageButtonStyle(drawable, drawable, drawable, drawable, drawable, drawable));
        idField.setText(file.nameWithoutExtension());
        setId(file.nameWithoutExtension());
        screen.save();
    }

    public void reload(){
        if(getItemname() != null)
            nameField.setText(getItemname());
        Gdx.app.log(getItemname(), nameField.getText());
        if(!getId().equals("null"))
            idField.setText(getId());
        priceField.setValue(getPrice());
        if(getNewName() != null) {
            newNameField.setText(getNewName());
        }
        Gdx.app.log(getNewName(), newNameField.getText());
        numField.setText(getNum() + "");
        //if(iconPath.equals("null") || iconPath.equals("")) return;

        Texture texture = null;

        String localPath = "";
        localPath += "good_bank" + "/" + id + ".png";

        if(Gdx.files.local(localPath).exists()){
            texture = new Texture(Gdx.files.local(localPath));
        } else{
            Gdx.app.log(Gdx.files.local(localPath).file().getAbsolutePath(), "IS NOT EXISTING");
        }

        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(texture));
        browseButton.setStyle(new VisImageButton.VisImageButtonStyle(drawable, drawable, drawable, drawable, drawable, drawable));
    }

    private void copyFile(FileHandle src, FileHandle dst){
        InputStream inStream;
        OutputStream outStream;

        try {
            if (!dst.parent().file().exists()) {
                dst.parent().mkdirs();
            }

            if (!dst.exists()) {
                dst.file().createNewFile();
            }

            inStream = new FileInputStream(src.file());
            outStream = new FileOutputStream(dst.file());

            byte[] buffer = new byte[1024];

            int length;
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }

            inStream.close();
            outStream.close();
        }
            catch(IOException exception){
                exception.printStackTrace();
            }
    }

    public void save(){
        setId(idField.getText());
        setItemname(nameField.getText());
        setNewName(newNameField.getText());
        setPrice(priceField.getValue());
    }

    public void setupWithXml(XmlReader.Element element){

        num = element.getInt("num");
        price = element.getInt("price");
        if(num < 245) {
            id = element.getChildByName("Captions").getChildByName("caption").getText();
            itemname = element.get("name");
        } else {
            itemname = element.getChildByName("Captions").getChildByName("caption").getText();
            id = element.get("name");
        }

        //reload();
    }

    public void setNewName(String newName){
        this.newName = newName;
    }

    public String getNewName(){
        return newName;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    @Override
    public int compareTo(ItemRow row) {
        if(getPrice() > row.getPrice()) return 1;
        else if(getPrice() < row.getPrice()) return -1;
        else return 0;
    }

    public static class Caption{
        public String lang, name;
        public Caption(String lang, String name){
            this.lang = lang;
            this.name = name;
        }
    }
}
