package com.kirill.gdxeditor.ashley.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.kirill.gdxeditor.ashley.components.PositionComponent;
import com.kirill.gdxeditor.ashley.components.RotationComponent;
import com.kirill.gdxeditor.ashley.components.SpriteComponent;

public class TransformSystem extends IteratingSystem {

	ComponentMapper<PositionComponent> pcm = ComponentMapper
			.getFor(PositionComponent.class);
	ComponentMapper<RotationComponent> rcm = ComponentMapper
			.getFor(RotationComponent.class);
	ComponentMapper<SpriteComponent> scm = ComponentMapper
			.getFor(SpriteComponent.class);

	public TransformSystem(Family family, int priority) {
		super(family);

	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		PositionComponent positionComponent = pcm.get(entity);
		RotationComponent rotationComponent = rcm.get(entity);
		SpriteComponent spriteComponent = scm.get(entity);
		
	}

}
