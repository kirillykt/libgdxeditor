package com.kirill.gdxeditor.ashley.systems;

import java.util.Comparator;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.kirill.gdxeditor.ashley.components.SpriteComponent;
import com.kirill.gdxeditor.ashley.components.TransformComponent;

public class RenderingSystem extends SortedIteratingSystem{

	SpriteBatch batch;
	static  ComponentMapper<SpriteComponent> scm = ComponentMapper.getFor(SpriteComponent.class);
	ComponentMapper<TransformComponent> tcm = ComponentMapper.getFor(TransformComponent.class);
	
	public RenderingSystem(Family family,int priority, SpriteBatch spriteBatch) {
		super(family, new PriorityComparator(), priority);
		batch = spriteBatch;
	}



	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		SpriteComponent spriteComponent = scm.get(entity);
		TransformComponent transformComponent = tcm.get(entity);
		spriteComponent.sprite.setPosition(transformComponent.position.x, transformComponent.position.y);
		spriteComponent.sprite.setRotation(transformComponent.rotation);
		spriteComponent.sprite.setSize(transformComponent.width, transformComponent.height);
		spriteComponent.sprite.setScale(transformComponent.scaleX, transformComponent.scaleY);
		spriteComponent.sprite.setOrigin(transformComponent.origin.x, transformComponent.origin.y);
		spriteComponent.sprite.draw(batch);
	}

	public static  class PriorityComparator implements Comparator<Entity>{
		@Override
		public int compare(Entity e1, Entity e2) {
			SpriteComponent spriteComponent1 = scm.get(e1);
			SpriteComponent spriteComponent2 = scm.get(e2);
			if(spriteComponent1.priority > spriteComponent2.priority) return 1;
			else if(spriteComponent1.priority < spriteComponent2.priority) return -1;
			else return 0;

		}
	}

}
