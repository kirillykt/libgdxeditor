package com.kirill.gdxeditor.ashley.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.kirill.gdxeditor.ashley.components.ScriptComponent;

public class ScriptingSystem extends IteratingSystem{
	
	ComponentMapper<ScriptComponent> scriptMapper = ComponentMapper.getFor(ScriptComponent.class);

	public ScriptingSystem(Family family, int priority) {
		super(family, priority);
		
	}
	
	

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		ScriptComponent script = scriptMapper.get(entity);
		if(script.isProcessing())
			script.update(deltaTime);
	}

}
