package com.kirill.gdxeditor.ashley.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.kirill.gdxeditor.Env;
import com.kirill.gdxeditor.ashley.EntityManager;
import com.kirill.gdxeditor.ashley.components.SpriteComponent;
import com.kirill.gdxeditor.ashley.components.TouchableComponent;
import com.kirill.gdxeditor.ashley.components.TransformComponent;
import com.kirill.gdxeditor.screens.EditorScreen;

public class InputSelectSystem extends IteratingSystem{
	ComponentMapper<TouchableComponent> touchMapper = ComponentMapper.getFor(TouchableComponent.class);
	ComponentMapper<TransformComponent> transformMapper = ComponentMapper.getFor(TransformComponent.class);
	EntityManager entityManager;
	OrthographicCamera camera;
	Family entityFamily;
	EditorScreen editorScreen;
	
	public InputSelectSystem(Family family, int priority, EntityManager entityManager){
		super(family, priority);
		entityFamily = family;
		this.entityManager = entityManager;
		camera = entityManager.mainCamera;
		editorScreen = (EditorScreen)Env.editor.getScreen(EditorScreen.class);
	}
	Vector2 deltaInput = new Vector2();
	
	Vector3 touchPoint = new Vector3();
	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		TouchableComponent touchableComponent = touchMapper.get(entity);
		TransformComponent transform = transformMapper.get(entity);
		touchableComponent.getBounds().setPosition(transform.position.x - touchableComponent.getBoundsScale(), transform.position.y - touchableComponent.getBoundsScale());

		Vector3 unproject = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
		long selectedId = 0;
		if(isLeftButtonJustTouched()){
			Vector3 tmpVec3 = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
			tmpVec3 = camera.unproject(tmpVec3);
			if(touchableComponent.contains(tmpVec3)){
				selectedId = entity.getId();
				for(int j = 0; j < getEntities().size(); j++){
					if(!getEntities().get(j).equals(entity))
						getEntities().get(j).getComponent(TouchableComponent.class).setSelected(false);
				}
				touchableComponent.setSelected(true);
				Entity selected = entity;
				touchPoint.x = tmpVec3.x;
				touchPoint.y = tmpVec3.y;
				touchPoint.x -= transform.position.x;
				touchPoint.y -= transform.position.y;
				if(selectedId != 0)
					editorScreen.inspectorWindow.onEntitySelect(selected);
				EntityManager.selectedId = entity.getId();
				Gdx.app.log("SELECTED", "" + entity.getId());
			}

		}
		
		if(touchableComponent.isSelected()){
			if(Gdx.input.isTouched() && Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
				deltaInput.x = Gdx.input.getDeltaX();
				deltaInput.y = -Gdx.input.getDeltaY();
				if(touchableComponent.contains(unproject)){
					touched = true;
				}
			}
		}
		
		if(touchableComponent.isSelected())
			dragSelected(transform, unproject);
		if(!Gdx.input.isTouched()) touched = false;
	}

	
	boolean touched;
	void dragSelected(TransformComponent transform, Vector3 unproject){
		if(!touched) return;
		//if(editorScreen.transformArrows.isActive()) return;
		Vector2 position = transform.position;
		position.set(unproject.x - touchPoint.x, unproject.y - touchPoint.y);
	}
	
	public boolean isLeftButtonJustTouched(){
		return Gdx.input.justTouched() && Gdx.input.isButtonPressed(Input.Buttons.LEFT);
	}
}
