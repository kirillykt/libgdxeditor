package com.kirill.gdxeditor.ashley.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.kirill.gdxeditor.ashley.components.interfaces.IScript;

public class ScriptComponent extends Component implements IScript{
	
	boolean isProcessing;
	IScript script;
	
	public ScriptComponent(Entity entity, IScript script){
		isProcessing = true;
		this.script = script;
	}

	@Override
	public void start(Entity entity) {
		script.start(entity);
	}

	@Override
	public void update(float delta) {
		script.update(delta);
	}

	@Override
	public boolean isProcessing() {
		return script.isProcessing();
	}

	@Override
	public void setProcessing(boolean processing) {
		script.setProcessing(processing);
	}

	@Override
	public void run(Runnable runnable) {
		runnable.run();
	}
	
	

}
