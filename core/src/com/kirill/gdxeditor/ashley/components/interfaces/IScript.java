package com.kirill.gdxeditor.ashley.components.interfaces;

import com.badlogic.ashley.core.Entity;

public interface IScript {
	public void start(Entity entity);
	public void update(float delta);
	public boolean isProcessing();
	public void setProcessing(boolean processing);
	public void run(Runnable runnable);
}
