package com.kirill.gdxeditor.ashley.components;

import com.badlogic.ashley.core.Component;

public class RotationComponent extends Component{
	public float rotation;
	
	public RotationComponent(){
		rotation = 0;
	}

}
