package com.kirill.gdxeditor.ashley.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class TransformComponent extends Component{
	
	public Vector2 position;
	public Vector2 origin;
	public Vector2 center;
	public float rotation;
	public float scaleX, scaleY;
	public float width, height;
	
	public TransformComponent(){
		position = new Vector2();
		origin = new Vector2();
		center = new Vector2();
		rotation = 0;
		scaleX = 1f;
		scaleY = 1f;
	}
	
	public void setSize(float width, float height){
		this.width = width;
		this.height = height;
	}
	
	public void setOrigin(float x, float y){
		origin.set(x, y);
	}
	
	public Vector2 getCenter(){
		center.set(position.x + width / 2, position.y + height / 2);
		return center;
	}

}
