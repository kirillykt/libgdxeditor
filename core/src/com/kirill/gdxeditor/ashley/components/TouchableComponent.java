package com.kirill.gdxeditor.ashley.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class TouchableComponent extends Component{
	private float boundsScl = 10f;
	private Rectangle bounds;
	private boolean isSelected = false;
	
	public TouchableComponent(Rectangle bounds) {

		this.bounds = new Rectangle(bounds);
		this.bounds.set(bounds.x - boundsScl, bounds.y - boundsScl, bounds.width + boundsScl * 2, bounds.height + boundsScl * 2);
		//touch = new Vector2();
	}
	
	public TouchableComponent(){
		bounds = new Rectangle();
	}
	
	public Rectangle getBounds(){
		return bounds;
	}
	
	public float getBoundsScale(){
		return boundsScl;
	}
	
	public boolean contains(float x, float y){
		return bounds.contains(x, y);
	}
	
	public boolean contains(Vector2 touch){
		return contains(touch.x, touch.y);
	}
	
	public boolean contains(Vector3 touch){
		return contains(touch.x, touch.y);
	}
	
	
	public void setSelected(boolean selected){
		isSelected = selected;
	}
	
	public boolean isSelected(){
		return isSelected;
	}
}
