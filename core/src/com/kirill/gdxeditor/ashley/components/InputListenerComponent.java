package com.kirill.gdxeditor.ashley.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Rectangle;

public class InputListenerComponent extends Component{
	public Rectangle bounds;
	public InputListenerComponent(Rectangle bounds){
		this.bounds = bounds;
	}
	
	public InputListenerComponent(){
		bounds = new Rectangle();
	}

}
