package com.kirill.gdxeditor.ashley.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class SpriteComponent extends Component{
	
	public Sprite sprite;
	public int priority;
	
	public SpriteComponent(int priority){
		sprite = new Sprite();
		this.priority = priority;
	}
	
	public SpriteComponent(Sprite sprite, int priority){
		this.sprite = sprite;
		this.priority = priority;
	}

}
