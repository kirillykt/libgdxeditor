package com.kirill.gdxeditor.ashley;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.kirill.gdxeditor.ashley.components.PositionComponent;
import com.kirill.gdxeditor.ashley.components.RotationComponent;
import com.kirill.gdxeditor.ashley.components.SpriteComponent;
import com.kirill.gdxeditor.ashley.components.TouchableComponent;
import com.kirill.gdxeditor.ashley.components.TransformComponent;
import com.kirill.gdxeditor.ashley.systems.InputSelectSystem;
import com.kirill.gdxeditor.ashley.systems.RenderingSystem;
import com.kirill.gdxeditor.ashley.systems.TransformSystem;

public class EntityManager{
	
	public Engine engine;
	public SpriteBatch batch;
	public OrthographicCamera mainCamera;
	public static long selectedId = 0;
	int currentPriority = 0;
	
	
	@SuppressWarnings("unchecked")
	public EntityManager(SpriteBatch batch, OrthographicCamera mainCamera){
		engine = new Engine();
		this.batch = batch;
		this.mainCamera = mainCamera;
		engine.addSystem(new TransformSystem(Family.all(PositionComponent.class, RotationComponent.class).get(), 1));
		engine.addSystem(new RenderingSystem(Family.all(TransformComponent.class, SpriteComponent.class).get(),  0, batch));
		engine.addSystem(new InputSelectSystem(Family.all(TouchableComponent.class, TransformComponent.class).get(), 2, this));
	}
	
	public void update(float delta){
		engine.update(delta);
	}
	
	public long createNewEntity(Sprite sprite){
		Entity entity = new Entity();
		
		entity.add(new TransformComponent()).add(new SpriteComponent(sprite, currentPriority));
		entity.getComponent(TransformComponent.class).setSize(sprite.getWidth(), sprite.getHeight());
		entity.getComponent(TransformComponent.class).position.set(sprite.getX(), sprite.getY());
		entity.getComponent(TransformComponent.class).origin.set(sprite.getWidth() / 2, sprite.getHeight() / 2);
		entity.add(new TouchableComponent(sprite.getBoundingRectangle()));
		entity.getComponent(TouchableComponent.class).setSelected(false);
		engine.addEntity(entity);
		currentPriority++;
		return entity.getId();
	}
	
	public Entity getSelectedEntity(){
		if(selectedId != 0)
			return engine.getEntity(selectedId);
		return null;
	}
	
	public void setSelectedEntity(long id){
		selectedId = id;
	}

}
